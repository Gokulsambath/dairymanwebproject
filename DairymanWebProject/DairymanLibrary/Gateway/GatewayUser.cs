﻿using DairymanLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DairymanLibrary.Gateway
{
   public  class GatewayUser
    {
        String strConnectionString;
        public GatewayUser()
        {
            strConnectionString = GatewayHelper.GetConnectionString();
        }
        public DataSet CheckUserLogin(UserEntities objInternaluserEntitie)        {            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("checkloginwithroleid", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.SelectCommand.Parameters.Add("@email", SqlDbType.NVarChar);            objSqlDataAdapter.SelectCommand.Parameters["@email"].Value = objInternaluserEntitie.Email;            objSqlDataAdapter.SelectCommand.Parameters.Add("@password", SqlDbType.NVarChar);
            objInternaluserEntitie.password = EncDec.Encrypt(objInternaluserEntitie.password, "passhide", "santosh", "SHA1", 4, "info1234hind5678", 192);
            objSqlDataAdapter.SelectCommand.Parameters["@password"].Value = objInternaluserEntitie.password;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;        }

        public DataSet GetAllVetDetailsByPincode()
        {
            string strCheckUserSQL = "GetAllVetDetailsByPincode";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void UpdatePassword(string password, string userid)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdatePassword", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;

            objSqlCommand.Parameters.Add("@userid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@userid"].Value = Convert.ToInt64(userid);
            
            objSqlCommand.Parameters.Add("@password", SqlDbType.NVarChar);
            password = EncDec.Encrypt(password, "passhide", "santosh", "SHA1", 4, "info1234hind5678", 192);
            objSqlCommand.Parameters["@password"].Value = password;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataTable GetTokenDetails(string tokenid, string date)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetTokenDetails", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.SelectCommand.Parameters.Add("@token", SqlDbType.NVarChar);
            objSqlDataAdapter.SelectCommand.Parameters["@token"].Value = tokenid;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void Insertpasswordresettoken(string userid,string token, DateTime tokenexpirydate, DateTime tokenexpireddate)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("Insertpasswordresettoken", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;

            objSqlCommand.Parameters.Add("@userid", SqlDbType.BigInt);
            objSqlCommand.Parameters["@userid"].Value = Convert.ToInt64(userid);
            objSqlCommand.Parameters.Add("@token", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@token"].Value = token;
            objSqlCommand.Parameters.Add("@expirydate", SqlDbType.SmallDateTime);
            objSqlCommand.Parameters["@expirydate"].Value = tokenexpirydate;
            objSqlCommand.Parameters.Add("@expireddate", SqlDbType.SmallDateTime);
            objSqlCommand.Parameters["@expireddate"].Value = tokenexpireddate;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet GetAllParavetDetailsByPincode()
        {
            string strCheckUserSQL = "GetAllParavetDetailsByPincode";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataTable CheckUserEmail(string email)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("checkuserEmail", objSqlConnection);            DataTable objDataSet = new DataTable();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;            objSqlDataAdapter.SelectCommand.Parameters.Add("@email", SqlDbType.NVarChar);            objSqlDataAdapter.SelectCommand.Parameters["@email"].Value = email;            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet GetAllAIDetailsByPincode()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllAIDetailsByPincode", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public DataSet GetAllConfigs()
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetAllConfigs", objSqlConnection);            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;                       objSqlDataAdapter.Fill(objDataSet);            return objDataSet;
        }

        public void UpdateAdminSuperAdminApprover(UserEntities objPriceRequestEntitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateAdminSuperAdminApprover", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@roleId", SqlDbType.Int);
            objSqlCommand.Parameters["@roleId"].Value = objPriceRequestEntitie.roleId;
            objSqlCommand.Parameters.Add("@firstName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@firstName"].Value = objPriceRequestEntitie.firstName;
            objSqlCommand.Parameters.Add("@lastName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@lastName"].Value = objPriceRequestEntitie.lastName;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = objPriceRequestEntitie.Email;
            objSqlCommand.Parameters.Add("@mobileNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mobileNo"].Value = objPriceRequestEntitie.mobileNo;
            objSqlCommand.Parameters.Add("@alternateNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@alternateNo"].Value = objPriceRequestEntitie.alternateNo;


            objSqlCommand.Parameters.Add("@approverfarmer", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverfarmer"].Value = objPriceRequestEntitie.ApproverFarmer;
            objSqlCommand.Parameters.Add("@approvervet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approvervet"].Value = objPriceRequestEntitie.ApproverVet;
            objSqlCommand.Parameters.Add("@approverparavet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverparavet"].Value = objPriceRequestEntitie.ApproverParavet;
            objSqlCommand.Parameters.Add("@approverait", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverait"].Value = objPriceRequestEntitie.ApproverAIT;

            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = objPriceRequestEntitie.userid;


            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public string GetImageURL(string Id,string name,int role)
        {
            SqlConnection connection = new SqlConnection(strConnectionString);            
            string sql = "";
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter("GetUserProfile", connection);

            objSqlDataAdapter.SelectCommand.Parameters.Add("@userid", SqlDbType.BigInt);            objSqlDataAdapter.SelectCommand.Parameters["@userid"].Value = Convert.ToInt32(Id);

            objSqlDataAdapter.SelectCommand.Parameters.Add("@roleId", SqlDbType.Int);            objSqlDataAdapter.SelectCommand.Parameters["@roleId"].Value = role;
           
            DataSet objDataSet = new DataSet();            objSqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objSqlDataAdapter.Fill(objDataSet);
            string urls = "";
            switch (name)
            {
                case "Voter":
                    urls =  objDataSet.Tables[0].Rows[0]["voterUrl"].ToString();
                    break;
                case "PAN":
                    urls =  objDataSet.Tables[0].Rows[0]["panUrl"].ToString();
                    break;
                case "Aadhar":
                    urls =  objDataSet.Tables[0].Rows[0]["aadharUrl"].ToString();
                    break;
                case "Profile":
                    urls =  objDataSet.Tables[0].Rows[0]["photoUrl"].ToString();
                    break;
                case "Education":
                    urls = objDataSet.Tables[0].Rows[0]["eduCertificateUrl"].ToString();
                    break;
            }
            
            return urls;
        }

        public DataSet GetAllDetailsForSuperAdmin()
        {
            string strCheckUserSQL = "GetAllDetailsForSuperAdmin";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void AddAdminSuperAdminApprover(UserEntities objPriceRequestEntitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddAdminSuperAdminApprover", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@roleId", SqlDbType.Int);
            objSqlCommand.Parameters["@roleId"].Value = objPriceRequestEntitie.roleId;
            objSqlCommand.Parameters.Add("@firstName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@firstName"].Value = objPriceRequestEntitie.firstName;
            objSqlCommand.Parameters.Add("@lastName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@lastName"].Value = objPriceRequestEntitie.lastName; 
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = objPriceRequestEntitie.Email;
            objSqlCommand.Parameters.Add("@mobileNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mobileNo"].Value = objPriceRequestEntitie.mobileNo;
            objSqlCommand.Parameters.Add("@alternateNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@alternateNo"].Value = objPriceRequestEntitie.alternateNo;
            objSqlCommand.Parameters.Add("@password", SqlDbType.NVarChar);
            objPriceRequestEntitie.password = EncDec.Encrypt(objPriceRequestEntitie.password, "passhide", "santosh", "SHA1", 4, "info1234hind5678", 192);

            objSqlCommand.Parameters["@password"].Value = objPriceRequestEntitie.password;

            objSqlCommand.Parameters.Add("@approverfarmer", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverfarmer"].Value = objPriceRequestEntitie.ApproverFarmer;
            objSqlCommand.Parameters.Add("@approvervet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approvervet"].Value = objPriceRequestEntitie.ApproverVet;
            objSqlCommand.Parameters.Add("@approverparavet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverparavet"].Value = objPriceRequestEntitie.ApproverParavet;
            objSqlCommand.Parameters.Add("@approverait", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverait"].Value = objPriceRequestEntitie.ApproverAIT;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public DataSet EditAdminDetails(int id)
        {
            string strCheckUserSQL = "EditAdminDetails";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.SelectCommand.Parameters.Add("@userid", SqlDbType.BigInt);
            objDataAdapter.SelectCommand.Parameters["@userid"].Value = id;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }


        public DataSet GetAllDetails()
        {
            string strCheckUserSQL = "GetAllDetails";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }



        public DataSet GetAllFarmerDetails()
        {
            string strCheckUserSQL = "GetAllFarmerDetailsWeb";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }


        public DataSet GetAllVetDetails()
        {
            string strCheckUserSQL = "GetAllVetDetails";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }


        public DataSet GetAllParavetDetails()
        {
            string strCheckUserSQL = "GetAllParavetDetails";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }


        public DataSet GetAllAIDetails()
        {
            string strCheckUserSQL = "GetAllAIDetails";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }


        public DataSet GetFarmerDetailsByUserid(string id)
        {
            string strCheckContactSQL = "GetFarmerDetailsByUserid";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckContactSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.SelectCommand.Parameters.Add("userid", SqlDbType.Int);
            objDataAdapter.SelectCommand.Parameters["userid"].Value = id;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public DataSet GetVetDetailsByUserid(string id)
        {
            string strCheckContactSQL = "GetVeternaryDetailsByUserid";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckContactSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.SelectCommand.Parameters.Add("userid", SqlDbType.Int);
            objDataAdapter.SelectCommand.Parameters["userid"].Value = id;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }
        public DataSet GetParavetDetailsByUserid(string id)
        {
            string strCheckContactSQL = "GetParavetDetailsByUserid";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckContactSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.SelectCommand.Parameters.Add("userid", SqlDbType.Int);
            objDataAdapter.SelectCommand.Parameters["userid"].Value = id;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }
        public DataSet GetAITDetailsByUserid(string id)
        {
            string strCheckContactSQL = "GetAITDetailsByUserid";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckContactSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.SelectCommand.Parameters.Add("userid", SqlDbType.Int);
            objDataAdapter.SelectCommand.Parameters["userid"].Value = id;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }



        public void ApproveFarmers(UserEntities objPriceRequestEntitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("ApproveFarmers", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = objPriceRequestEntitie.userid;
            objSqlCommand.Parameters.Add("@roleId", SqlDbType.Int);
            objSqlCommand.Parameters["@roleId"].Value = objPriceRequestEntitie.roleId;
            objSqlCommand.Parameters.Add("@comments", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@comments"].Value = objPriceRequestEntitie.comments;

    


            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }




        public void RejectFarmers(UserEntities objPriceRequestEntitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("RejectFarmers", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = objPriceRequestEntitie.userid;
            objSqlCommand.Parameters.Add("@roleId", SqlDbType.Int);
            objSqlCommand.Parameters["@roleId"].Value = objPriceRequestEntitie.roleId;
            objSqlCommand.Parameters.Add("@comments", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@comments"].Value = objPriceRequestEntitie.comments;



            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }






    }
}
