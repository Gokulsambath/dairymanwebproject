﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DairymanLibrary.Gateway
{
    class GatewayLookup
    {
        String strConnectionString;
        public GatewayLookup()
        {
            strConnectionString = GatewayHelper.GetConnectionString();
        }
        public DataSet GetAllConfigs(string pstrConfigType)
        {
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter("GetAllConfigs", objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.SelectCommand.Parameters.Add("@configtype", SqlDbType.NChar);
            objDataAdapter.SelectCommand.Parameters["@configtype"].Value = pstrConfigType;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }
    }
}
