﻿using DairymanLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DairymanLibrary.Gateway
{
    public class GatewayApprover
    {

        String strConnectionString;
        public GatewayApprover()
        {
            strConnectionString = GatewayHelper.GetConnectionString();
        }
        public DataSet GetAllDetailsApprover()
        {
            string strCheckUserSQL = "GetAllDetailsforApprover";
            SqlConnection objConnection = new SqlConnection(strConnectionString);
            SqlDataAdapter objDataAdapter = new SqlDataAdapter(strCheckUserSQL, objConnection);
            DataSet objDataSet = new DataSet();
            objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            objDataAdapter.Fill(objDataSet);
            return objDataSet;
        }

        public void UpdateApprover(UserEntities objPriceRequestEntitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("UpdateApprover", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@roleId", SqlDbType.Int);
            objSqlCommand.Parameters["@roleId"].Value = objPriceRequestEntitie.roleId;
            objSqlCommand.Parameters.Add("@firstName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@firstName"].Value = objPriceRequestEntitie.firstName;
            objSqlCommand.Parameters.Add("@lastName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@lastName"].Value = objPriceRequestEntitie.lastName;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = objPriceRequestEntitie.Email;
            objSqlCommand.Parameters.Add("@mobileNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mobileNo"].Value = objPriceRequestEntitie.mobileNo;
            objSqlCommand.Parameters.Add("@alternateNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@alternateNo"].Value = objPriceRequestEntitie.alternateNo;
            
            objSqlCommand.Parameters.Add("@approverfarmer", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverfarmer"].Value = objPriceRequestEntitie.ApproverFarmer;
            objSqlCommand.Parameters.Add("@approvervet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approvervet"].Value = objPriceRequestEntitie.ApproverVet;
            objSqlCommand.Parameters.Add("@approverparavet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverparavet"].Value = objPriceRequestEntitie.ApproverParavet;
            objSqlCommand.Parameters.Add("@approverait", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverait"].Value = objPriceRequestEntitie.ApproverAIT;

            objSqlCommand.Parameters.Add("@userid", SqlDbType.Int);
            objSqlCommand.Parameters["@userid"].Value = objPriceRequestEntitie.userid;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }

        public void AddApprover(UserEntities objPriceRequestEntitie)
        {
            SqlConnection objSqlConnection = new SqlConnection(strConnectionString);
            SqlCommand objSqlCommand = new SqlCommand("AddApprover", objSqlConnection);
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add("@roleId", SqlDbType.Int);
            objSqlCommand.Parameters["@roleId"].Value = objPriceRequestEntitie.roleId;
            objSqlCommand.Parameters.Add("@firstName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@firstName"].Value = objPriceRequestEntitie.firstName;
            objSqlCommand.Parameters.Add("@lastName", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@lastName"].Value = objPriceRequestEntitie.lastName;
            objSqlCommand.Parameters.Add("@email", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@email"].Value = objPriceRequestEntitie.Email;
            objSqlCommand.Parameters.Add("@mobileNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@mobileNo"].Value = objPriceRequestEntitie.mobileNo;
            objSqlCommand.Parameters.Add("@alternateNo", SqlDbType.NVarChar);
            objSqlCommand.Parameters["@alternateNo"].Value = objPriceRequestEntitie.alternateNo;
            objSqlCommand.Parameters.Add("@password", SqlDbType.NVarChar);
            objPriceRequestEntitie.password = EncDec.Encrypt(objPriceRequestEntitie.password, "passhide", "santosh", "SHA1", 4, "info1234hind5678", 192);

            objSqlCommand.Parameters["@password"].Value = objPriceRequestEntitie.password;

            objSqlCommand.Parameters.Add("@approverfarmer", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverfarmer"].Value = objPriceRequestEntitie.ApproverFarmer;
            objSqlCommand.Parameters.Add("@approvervet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approvervet"].Value = objPriceRequestEntitie.ApproverVet;
            objSqlCommand.Parameters.Add("@approverparavet", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverparavet"].Value = objPriceRequestEntitie.ApproverParavet;
            objSqlCommand.Parameters.Add("@approverait", SqlDbType.VarChar);
            objSqlCommand.Parameters["@approverait"].Value = objPriceRequestEntitie.ApproverAIT;

            objSqlConnection.Open();
            objSqlCommand.ExecuteNonQuery();
            objSqlConnection.Close();
        }
    }
}
