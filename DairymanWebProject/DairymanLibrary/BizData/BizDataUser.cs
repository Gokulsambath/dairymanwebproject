﻿using DairymanLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DairymanLibrary.Gateway;

namespace DairymanLibrary.BizData
{
    public class BizDataUser
    {
        public DataSet CheckUserLogin(UserEntities objInternaluserEntitie)        {            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.CheckUserLogin(objInternaluserEntitie);        }

        public void ApproveFarmers(UserEntities objInternaluserEntitie)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();
            objGatewayInternaluser.ApproveFarmers(objInternaluserEntitie);
        }

        public DataTable GetTokenDetails(string tokenid, string date)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.GetTokenDetails(tokenid,date);
        }

        public DataSet GetAllVetDetailsByPincode()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllVetDetailsByPincode();
        }

        public DataTable CheckUserEmail(string email)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            return objGatewayInternaluser.CheckUserEmail(email);
        }

        public void UpdatePassword(string password, string userid)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();            objGatewayInternaluser.UpdatePassword(password,userid);
        }

        public void RejectFarmers(UserEntities objInternaluserEntitie)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();
            objGatewayInternaluser.RejectFarmers(objInternaluserEntitie);
        }

        public void Insertpasswordresettoken(string userid, string token, DateTime tokenexpirydate, DateTime tokenexpireddate)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();
            objGatewayInternaluser.Insertpasswordresettoken(userid,token,tokenexpirydate,tokenexpireddate);
        }

        public void AddAdminSuperAdminApprover(UserEntities objInternaluserEntitie)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();
            objGatewayInternaluser.AddAdminSuperAdminApprover(objInternaluserEntitie);
        }

        public DataSet GetAllConfigs()
        {

            GatewayUser objGatewayInternaluser = new GatewayUser();
            return objGatewayInternaluser.GetAllConfigs();
        }

        public void UpdateAdminSuperAdminApprover(UserEntities objInternaluserEntitie)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();
            objGatewayInternaluser.UpdateAdminSuperAdminApprover(objInternaluserEntitie);
        }
        public DataSet EditAdminDetails(int id)
        {
            GatewayUser objGatewayInternaluser = new GatewayUser();
            return objGatewayInternaluser.EditAdminDetails(id);
        }

        public DataSet GetAllDetails()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllDetails();
        }

        public DataSet GetAllFarmerDetails()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllFarmerDetails();
        }

        public DataSet GetAllParavetDetailsByPincode()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllParavetDetailsByPincode();
        }

        public DataSet GetAllVetDetails()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllVetDetails();
        }


        public DataSet GetAllParavetDetails()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllParavetDetails();
        }

        public DataSet GetAllAIDetails()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllAIDetails();
        }

        public DataSet GetFarmerDetailsByUserid(string id)
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetFarmerDetailsByUserid(id);
        }
        public DataSet GetVetDetailsByUserid(string id)
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetVetDetailsByUserid(id);
        }

        public string GetImageURL(string sometin,string name,int role)
        {
            GatewayUser objuserdetailGateway = new GatewayUser();
            return objuserdetailGateway.GetImageURL(sometin,name,role);
        }

        public DataSet GetParavetDetailsByUserid(string id)
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetParavetDetailsByUserid(id);
        }
        public DataSet GetAITDetailsByUserid(string id)
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAITDetailsByUserid(id);
        }

        public DataSet GetAllDetailsForSuperAdmin()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllDetailsForSuperAdmin();
        }

        public DataSet GetAllAIDetailsByPincode()
        {
            GatewayUser objUserdetailsGateway = new GatewayUser();
            return objUserdetailsGateway.GetAllAIDetailsByPincode();
        }
    }
}
