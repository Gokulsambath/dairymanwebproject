﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DairymanLibrary.Gateway;

namespace DairymanLibrary.BizData
{
    public class BizDataLookup
    {
        public DataSet GetAllConfigs(string pstrConfigType)
        {
            GatewayLookup objGatewayLookups = new GatewayLookup();
            return objGatewayLookups.GetAllConfigs(pstrConfigType);
        }
    }
}
