﻿using DairymanLibrary.Entity;
using DairymanLibrary.Gateway;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DairymanLibrary.BizData
{
    public class BizDataApprover
    {
       
        public DataSet GetAllDetailsApprover()
        {
            GatewayApprover objUserdetailsGateway = new GatewayApprover();
            return objUserdetailsGateway.GetAllDetailsApprover();
        }

        public void AddApprover(UserEntities sourcelead)
        {
            GatewayApprover objGatewayInternaluser = new GatewayApprover();
            objGatewayInternaluser.AddApprover(sourcelead);
        }

        public void UpdateApprover(UserEntities sourcelead)
        {
            GatewayApprover objGatewayInternaluser = new GatewayApprover();
            objGatewayInternaluser.UpdateApprover(sourcelead);
        }
    }
}
