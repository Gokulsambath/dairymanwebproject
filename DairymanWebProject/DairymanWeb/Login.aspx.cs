﻿using DairymanLibrary.BizData;
using DairymanLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DairymanWeb
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btneditsubmit_Click(object sender, EventArgs e)
        {

            string strEmail = Email.Value.Trim();
            string strPassword = password.Value.Trim();
            UserEntities objInternaluserEntitie = new UserEntities();
            objInternaluserEntitie.Email = strEmail;
            objInternaluserEntitie.password = strPassword;

            DataSet dsInternaluser = new BizDataUser().CheckUserLogin(objInternaluserEntitie);
            if (dsInternaluser.Tables[0].Rows.Count > 0)
            {
                Session["LoggedIn"] = "Y";
                Session["UserType"] = dsInternaluser.Tables[0].Rows[0]["roleName"].ToString(); ;
                Session["UserTypeID"] = dsInternaluser.Tables[0].Rows[0]["roleid"].ToString();
                Session["UserName"] = dsInternaluser.Tables[0].Rows[0]["email"].ToString();
                Session["UserEmail"] = dsInternaluser.Tables[0].Rows[0]["email"].ToString();
                Session["UserID"] = dsInternaluser.Tables[0].Rows[0]["userid"].ToString();
                Session["FullName"] = dsInternaluser.Tables[0].Rows[0]["FullName"].ToString();
                if (Session["UserTypeID"].ToString() == "6")
                {                    
                    Response.Redirect("Data.aspx?Action=AdminDashboard");
                }
                else if(Session["UserTypeID"].ToString()=="7")
                {
                    Response.Redirect("Data.aspx?Action=SuperAdminDashboard");

                }
                else if (Session["UserTypeID"].ToString() == "8")
                {
                    Response.Redirect("Data.aspx?Action=ApproverDashboard");

                }

            }
            else
            {
                lblLoginError.Visible = true;
                lblLoginError.Text = "User Id or Password is wrong";
            }
        }

    
    }
}