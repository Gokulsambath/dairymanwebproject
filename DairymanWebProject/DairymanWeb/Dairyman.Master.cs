﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


namespace DairymanWeb
{
    public partial class Dairyman : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)

        {
            Control ctlHeader = LoadControl("~/UserControls/GlobalHeaderLoggedIn.ascx");            GlobalHeaderContent.Controls.Add(ctlHeader);            Control ctlHome = LoadControl("~/UserControls/GlobalHeader.ascx");            MiddleCenterContent.Controls.Add(ctlHome);

            Control ctlFoot = LoadControl("~/UserControls/GlobalFooter.ascx");            GlobalFooterContent.Controls.Add(ctlFoot);


            //MiddleContent


            if (this.Page.Request.QueryString.Get("Action").ToString().Equals("AdminDashboard"))            {                Control ctlAdminDashboard = LoadControl("~/UserControls/AdminDashboard.ascx");                MiddleCenterContent.Controls.Add(ctlAdminDashboard);            }
          else  if (this.Page.Request.QueryString.Get("Action").ToString().Equals("SuperAdminDashboard"))            {                Control ctlSuperAdminDashboard = LoadControl("~/UserControls/SuperAdminDashboard.ascx");                MiddleCenterContent.Controls.Add(ctlSuperAdminDashboard);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("ApproverDashboard"))            {                Control ctlApproverDashboard = LoadControl("~/UserControls/ApproverDashboard.ascx");                MiddleCenterContent.Controls.Add(ctlApproverDashboard);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("UserDetails"))            {                Control ctlUserDetails = LoadControl("~/UserControls/UserDetails.ascx");                MiddleCenterContent.Controls.Add(ctlUserDetails);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("FarmerDetails"))            {                Control ctlFarmerDetails = LoadControl("~/UserControls/FarmerDetails.ascx");                MiddleCenterContent.Controls.Add(ctlFarmerDetails);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("VetDetails"))            {                Control ctlVetDetails = LoadControl("~/UserControls/VetDetails.ascx");                MiddleCenterContent.Controls.Add(ctlVetDetails);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("ParavetDetails"))            {                Control ctlParavetDetails = LoadControl("~/UserControls/ParavetDetails.ascx");                MiddleCenterContent.Controls.Add(ctlParavetDetails);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("AIDetails"))            {                Control ctlAIDetails = LoadControl("~/UserControls/AIDetails.ascx");                MiddleCenterContent.Controls.Add(ctlAIDetails);            }

            else if (this.Page.Request.QueryString.Get("Action").ToString().Equals("Pincode"))            {                Control ctlAIDetails = LoadControl("~/UserControls/pincodedetails.ascx");                MiddleCenterContent.Controls.Add(ctlAIDetails);            }


        }
    }
}