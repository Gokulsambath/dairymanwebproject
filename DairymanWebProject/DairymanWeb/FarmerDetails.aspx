﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FarmerDetails.aspx.cs" Inherits="DairymanWeb.FarmerDetails" %>



    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">Farmer Details</h5>
              </div>     
            </div>
          </div>
        </div>

        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">


        <div class="row animate fadeUp">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
                  <form>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="farmarid" class="validate">
                      <label for="farmarid">Farmer ID</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="firstname" class="validate">
                      <label for="firstname">Firstname</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="lastname" class="validate">
                      <label for="lastname">Lastname</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="email" class="validate">
                      <label for="email">Email</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="tel" id="mobile" class="validate">
                      <label for="mobile">Mobile</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="tel" id="alternateno" class="validate">
                      <label for="alternateno">Alternate No</label>
                    </div>
                    <div class="input-field col s12">
                      <textarea id="address" class="materialize-textarea"></textarea>
                      <label for="address">Address</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="village" class="validate">
                      <label for="village">Village</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="hamlet" class="validate">
                      <label for="hamlet">Hamlet</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="taluka" class="validate">
                      <label for="taluka">Taluka / Tehsil</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="dist" class="validate">
                      <label for="dist">District</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="pin" class="validate">
                      <label for="pin">Pin Code</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="state" class="validate">
                      <label for="state">State</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="pan" class="validate">
                      <label for="pan">PAN Number</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="aadhar" class="validate">
                      <label for="aadhar">Aadhar Number</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="aadhar" class="validate">
                      <label for="aadhar">Voter ID</label>
                    </div>
                    <div class="col x12 s12 m12 l12 mt-2 mb-2">
                      <div class="row">
                        <div class="col s12 m6 l6">
                          <div class="col s12 m12 l12">
                              <p>Upload your voter ID proof</p>
                          </div>
                          <div class="col s12 m12 l12 mt-3">
                            <input type="file" id="input-file-now" class="dropify" data-default-file="" />
                          </div>
                        </div> 
                        <div class="col s12 m6 l6">
                          <div class="col s12 m12 l12">
                              <p>Upload your profile proof</p>
                          </div>
                          <div class="col s12 m12 l12 mt-3">
                            <input type="file" id="input-file-now" class="dropify" data-default-file="" />
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="col x12 s12 m12 l12">
                      <div class="divider"></div>
                      <div class="step-actions modal-footer pt-2 mb-2" style="text-align: center;">
                        <span class="btn btn-sm btn-primary gradient-45deg-green-teal" onclick="showcomment()">Approve</span>
                        <a href="#rejected" class="modal-trigger"><span class="btn btn-sm btn-primary gradient-45deg-deep-orange-orange">Reject</span></a>
                        <span class="gradient-45deg-red-pink btn btn-sm btn-secondary" onclick="showcomment()">Comments</span>
                      </div>
                    </div>
                    <div class="col x12 s12 m12 l12" style="display: none;" id="showcomment">
                      <div class="divider"></div>
                      <div class="input-field col s12">
                        <textarea id="commentbox" class="materialize-textarea"></textarea>
                        <label for="">Comment Box</label>
                      </div>
                      <div class="step-actions modal-footer pt-2 mb-2" style="text-align: center;">
                        <!-- <button class="modal-close waves-effect waves-dark btn btn-sm btn-primary gradient-45deg-deep-orange-orange">Cancel</button> -->
                        <button class="btn btn-sm btn-secondary" type="submit" name="action">Submit</button>
                      </div>
                    </div> 
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>



          </div>
        </div>
      </div>
    </div>
    <!-- END: Page Main-->

    
