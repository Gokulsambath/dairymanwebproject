﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DairymanWeb.Startup))]
namespace DairymanWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
