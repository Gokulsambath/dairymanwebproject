﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DairymanWeb.Login" %>


<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Login</title>
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/login-page-logo-img.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/login.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/custom/custom.css">
    <!-- END: Custom CSS-->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <style type="text/css">
      .logo-img
      {
        margin-top: 35px;
        width: 70%;
      }
      #logodiv
      {
        height: 425px; 
        position: relative; 
        left: -15%; 
        opacity: 0.6;
      }

      #lblLoginError{
          color:red;
      }
      #formdiv
      {
        height:380px;  
        position: absolute; 
        right: 15%; 
        top: 19.5%;
      }

      @media only screen and (max-width: 576px)
      {
           #logodiv
            {
              height: auto;
              position: fixed; 
              left: auto;
            }
            #formdiv
            {
              height:450px;
              position: fixed; 
              right: auto; 
              top: auto;
            }
            .transform
            {
              text-shadow: none;
            }
      }
     
    </style>
    

  <script type="text/javascript">
        function HideLabel() {
            document.getElementById('<%= lblLoginError.ClientID %>').style.display = "none";
        }
        setTimeout("HideLabel();", 4000);
   
    </script>
  </head>
  <!-- END: Head-->
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 1-column login-b  blank-page blank-page" data-open="click" data-menu="vertical-modern-menu" data-col="1-column">
    <div class="row animate fadeUp">
      <div class="col s12">
        <div class="container">
        	<div id="login-page" class="row">

        		<div class="col s12 m5 l5 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8 center-align" id="logodiv" >
                <div class="row animate fadeLeft">
                  <div class="col s12 m12 l12">
                    <img src="app-assets/images/login-page-logo-img.png" class="logo-img">   
                  </div>
                </div>
        		</div>

				    <div class="col s12 m4 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8" id="formdiv">
  				    <form class="login-form" runat="server">
  				      <div class="row animate fadeRight">
  				        <div class="input-field col s12">
  				          <h5 class="ml-10">Sign in</h5>
  				        </div>
  				      </div>
  				      <div class="row mt-5">
  				        <div class="input-field col s12 mt-0">
  				          <i class="material-icons prefix pt-2">person_outline</i>
  				          <input id="Email" runat="server" type="email" name="Email" required>
  				          <label for="username" runat="server" class="center-align" maxlength="50">Username</label>
  				        </div>
  				      </div>
  				      <div class="row">
  				        <div class="input-field col s12 mt-0">
  				          <i class="material-icons prefix pt-2">lock_outline</i>
  				          <input id="password" runat="server" type="password" maxlength="50" name="password" required>
  				          <label for="password" runat="server">Password</label>
  				        </div>
  				      </div>
<!--   				      <div class="row">
  				        <div class="col s12 m12 l12 ml-2 mt-0">
  				          <p class="ml-2 mt-0">
  				            <label>
  				              <input type="checkbox" />
  				              <span>keep me logged-in</span>
  				            </label>
  				          </p>
  				        </div>
  				      </div> -->
  				      <div class="row mt-5">
  				        <div class="input-field col s12">
  				      
  				   
                     <asp:Button ID="btneditsubmit" runat="server" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" Text="Login" OnClick="btneditsubmit_Click" />           
                              
                                </div>
  				      </div>
                          <div class="col-md-12">                            <asp:Label ID="lblLoginError" runat="server" CssClass="danger" Visible="false"></asp:Label>                        </div>
  				      <div class="row">
  				        <!-- <div class="input-field col s6 m6 l6">
  				          <p class="margin medium-small"><a href="register.html">Register Now!</a></p>
  				        </div> -->
  				        <div class="input-field col s12 m12 l12 right-align mt-1">
  				          <p class="margin medium-small"><a href="ForgotPassword.aspx">Forgot password ?</a></p>
  				        </div>
  				      </div>
  				    </form>
				  </div>

				  
                
                </div>
              </div>
            </div>
          </div>

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="app-assets/js/plugins.js" type="text/javascript"></script>
    <script src="app-assets/js/custom/custom-script.js" type="text/javascript"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
  </body>

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/vertical-modern-menu-template/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 08 Apr 2019 05:04:28 GMT -->
</html>