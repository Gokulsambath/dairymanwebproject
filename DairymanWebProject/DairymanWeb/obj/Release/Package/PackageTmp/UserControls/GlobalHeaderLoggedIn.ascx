﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalHeaderLoggedIn.ascx.cs" Inherits="DairymanWeb.UserControls.GlobalHeaderLoggedIn" %>



<script>
    $(document).ready(function () {        
        var me = "Data?Action="+ getUrlVars()["Action"] +"";      
        var anchors = $('li.bold a');        
        for (var i = 0, l = $('li.bold a').length; i < l; i++) {
             $(anchors[i]).removeClass("active");
            var orig ="Data?Action="+  geturl(anchors[i].href)["Action"]+"";
            if (me == orig) {                
                $(anchors[i]).addClass("active");
            }
        }
        
    });

    //get param for passed url
    function geturl(actionsss)
    {
        var vars = [], hash;
        var hashes = actionsss.slice(actionsss.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    //get param for current url
    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
</script>


<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
     <div class="brand-sidebar">
       <h1 class="logo-wrapper"><a class="darken-1" href="index.html"><img src="app-assets/images/login-page-logo-img.png" alt=" logo" class="logo-img" style=""/ ><span class="logo-text hide-on-med-and-down"></span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
     </div>
     <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
        <%                
    if (Session["UserTypeID"].ToString() == "7")
    {
        %>
         <li class="bold"><a class="waves-effect waves-cyan" href="Data.aspx?Action=SuperAdminDashboard"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="">Dashboard</span></a></li>
         <%                
    }
        %>
              <%                
    if (Session["UserTypeID"].ToString() == "6")
    {
        %>
         <li class="bold"><a class="waves-effect waves-cyan" href="Data.aspx?Action=AdminDashboard"><i class="material-icons dp48">face</i><span class="menu-title" data-i18n="">Dashboard</span></a></li>
         <%                
    }
        %>
              <%                
    if (Session["UserTypeID"].ToString() == "8")
    {
        %>
         <li class="bold"><a class="waves-effect waves-cyan" href="Data.aspx?Action=ApproverDashboard"><i class="material-icons dp48">face</i><span class="menu-title" data-i18n="">Dashboard</span></a></li>

        
         <%                
    }
        %>
          <li class="bold"><a class="waves-effect waves-cyan" href="Data.aspx?Action=UserDetails"><i class="material-icons dp48">face</i><span class="menu-title" data-i18n="">User Details</span></a></li>
          <li class="bold"><a class="waves-effect waves-cyan" href="Data.aspx?Action=Pincode"><i class="material-icons dp48">face</i><span class="menu-title" data-i18n="">Pincode Details</span></a></li>
    </ul>
     <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
 </aside>
   <!-- END: SideNav-->
