﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalFooter.ascx.cs" Inherits="DairymanWeb.UserControls.GlobalFooter" %>
 <!-- BEGIN: Footer-->
      <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
        <div class="footer-copyright">
          <div class="container" style="text-align:center"><span>2019 &copy; Dairy Man by  Innovellent</span></div>
        </div>
      </footer>
    <!-- End: Footer-->