﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FarmerDetails.ascx.cs" Inherits="DairymanWeb.UserControls.FarmerDetails" %>



  

    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">Farmer Details</h5>
              </div>     
            </div>
          </div>
        </div>

        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">


        <div class="row animate fadeUp">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
                <%--  <form  runat="server" novalidate>--%>
<%--                <asp:HiddenField ID="roleid" runat="server" />  --%>

                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="farmarid" runat="server" class="validate" readonly="readonly">
                      <label for="farmarid">Farmer ID</label>

                    </div>


                        <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="roleid" class="validate" runat="server" readonly="readonly">
                      <label for="roleid">Role ID</label>

                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="firstname" class="validate" runat="server">
                      <label for="firstname">Firstname</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="lastname" class="validate" runat="server">
                      <label for="lastname">Lastname</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="email" class="validate"  runat="server">
                      <label for="email">Email</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="tel" id="mobile" class="validate" runat="server">
                      <label for="mobile">Mobile</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="tel" id="alternateno" class="validate" runat="server">
                      <label for="alternateno">Alternate No</label>
                    </div>
                    <div class="input-field col s12">
                      <textarea id="address" class="materialize-textarea" runat="server"></textarea>
                      <label for="address">Address</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="village" class="validate" runat="server">
                      <label for="village">Village</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="hamlet" class="validate" runat="server">
                      <label for="hamlet">Hamlet</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="taluka" class="validate" runat="server">
                      <label for="taluka">Taluka / Tehsil</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="dist" class="validate" runat="server">
                      <label for="dist">District</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="pin" class="validate" runat="server">
                      <label for="pin">Pin Code</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="state" class="validate" runat="server">
                      <label for="state">State</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="pan" class="validate" runat="server">
                      <label for="pan">PAN Number</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="aadhar" class="validate" runat="server">
                      <label for="aadhar">Aadhar Number</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="voter" class="validate" runat="server">
                      <label for="aadhar">Voter ID</label>
                    </div>
                   
                    <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink1" runat="server">
                          <div class="row">
                            <div class="col s12 m4 l3">
                                <p>Upload your ID proof</p>
                              </div>
                              <div class="col s12 m8 l9">
                            
                                   <a runat="server" id="inputfilelink1" target="_blank">View Photo</a>
                              </div>
                            </div>

                         </div> 
                    <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink2" runat="server">
                          <div class="row">
                            <div class="col s12 m4 l3">
                                  <p>Uploaded PAN proof</p>
                              </div>
                              <div class="col s12 m8 l9">
                            
                                  <a runat="server" id="inputfilelink2" target="_blank">View Photo</a>
                              </div>
                            </div>

                         </div> 
                    <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink3" runat="server">
                      <div class="row">
                        <div class="col s12 m4 l3">
                            <p>Uploaded Aadhar</p>
                        </div>
                        <div class="col s12 m8 l9">
                          <a runat="server" ID="inputfilelink3" target="_blank">View Photo</a>
                        </div> 
                      </div>
                    </div>
                   <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink4" runat="server">
                      <div class="row">
                        <div class="col s12 m4 l3">
                            <p>Uploaded Profile Photo</p>
                        </div>
                        <div class="col s12 m8 l9">
                          <a runat="server" ID="inputfilelink4" target="_blank">View Photo</a>
                        </div> 
                      </div>
                    </div>

                    <div class="col x12 s12 m12 l12">
                      <div class="divider"></div>
                      <div class="step-actions modal-footer pt-2 mb-2" style="text-align: center;">
           
                      </div>
                    </div>
                    <div class="col x12 s12 m12 l12"  id="showcomment">
                      <div class="divider"></div>
                      <div class="input-field col s12">
                        <textarea id="commentbox" class="materialize-textarea" runat="server"></textarea>
                        <label for="">Comment Box</label>
                      </div>
                      <div class="step-actions modal-footer pt-2 mb-2" style="text-align: center;">
                       
                   <asp:Button ID="btnSubmit" runat="server" class="btn btn-sm btn-primary gradient-45deg-green-teal" onclick="btnSubmit_Click"  Text="Approve"  />
                     
                    <asp:Button ID="BtnReject" runat="server" class="btn btn-sm  gradient-45deg-deep-orange-orange" onclick="BtnReject_Click"  Text="Reject"  />
                                    
                      </div>
                    </div> 
              
              </div>
            </div>
          </div>
        </div>
      </div>

           



          </div>
        </div>
      </div>
    </div>
    <!-- END: Page Main-->

    
 <!---------------------------All  Modal ------------------------------>
          <!----- Alert Modal Start--->
            <div id="rejected" class="modal alert-modal" runat="server">
              <div class="modal-content pt-0">
                <!-- <span class="modal-header right modal-close">
                  <i class="material-icons right-align">clear</i>
                </span> -->
                <!-- <h5 style="font-size:22px;text-align:left; color: #5c6bc0;">
                  <strong>Add User</strong>
                </h5> -->
                <!-- <div class="divider"></div> -->
<%--                <form action="" method="">--%>
                  <div class="row">
                    <div class="col m12 l12 mt-1">
                       <h5 style="font-size:22px;text-align:center; color: #5c6bc0;">
                        <strong>Are you Sure ?</strong>
                      </h5>
                    </div>  
                  </div>
                  <div class="step-actions modal-footer" style=" text-align: center;">
                    <span class="modal-close waves-effect waves-dark btn btn-sm btn-primary gradient-45deg-deep-orange-orange"><i class="material-icons">close</i></span>
                    <span class="modal-close btn btn-sm btn-secondary gradient-45deg-green-teal"  onclick="showcomment()"><i class="material-icons">done</i></span>
                    <p style="color:#e57373;">***If yes, You have to provide proper reason***</p>
                  </div>
<%--                </form>--%>
              </div>
            </div>
            <!----- Alert Modal End--->
            <!---------------------------All  Modal ------------------------------>



<script src="app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
    $("#BtnReject").click(function () {
        $("#rejected").modal();
  });
});
</script>
