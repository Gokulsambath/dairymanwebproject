﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApproverDashboard.ascx.cs" Inherits="DairymanWeb.UserControls.ApproverDashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style>
    [type='checkbox']:not(:checked), [type='checkbox']:checked{
        opacity:1 !important;
        position:relative!important;
    }
   
</style>
<script type="text/javascript">    function ValidateCheckBoxList(sender, args) {        var checkBoxList = document.getElementById("<%=chkRole.ClientID %>");        var checkboxes = checkBoxList.getElementsByTagName("input");        var isValid = false;        for (var i = 0; i < checkboxes.length; i++) {            if (checkboxes[i].checked) {                isValid = true;                break;            }        }        args.IsValid = isValid;    }</script>

    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">Approver Dashboard</h5>
              </div>
            </div>
          </div>
        </div>

        <!----------------------------------------- table start ---------------------------------->
        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">
  
                <!-- Page Length Options -->
                <div class="row animate fadeUp">
                  <div class="col s12">
                    <div class="card">
                      <div class="card-content" style="">
                        <div class="row">
                          <div class="col s12">
                            <table id="page-length-option" class="display striped centered" style="width:100%">
                              <thead>
                                <tr>
                                  <th>Role</th>
                                  <th>ID</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Email</th>
                                  <th>Mobile</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                  <asp:PlaceHolder ID="pldApproverDashboard" runat="server"></asp:PlaceHolder>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

</div>
              <!-- -------------------------------table end ---------------------------------------->

<!---------------------------All  Modal ------------------------------>
          <!----- Add User Modal Start--->
            <div id="adduserr" class="modal">
              <div class="modal-content pt-0">
                <span class="modal-header right modal-close">
                  <i class="material-icons right-align">clear</i>
                </span>
                <h5 style="font-size:22px;text-align:left; color: #5c6bc0;">
                  <strong><span id="addedit" runat="server">Add User</span></strong>
                </h5>
                <div class="divider"></div>
               <%-- <asp:ScriptManager ID="toolscriptmanager" runat="server"></asp:ScriptManager>--%>
                  <div class="row">
                  <%--  <asp:UpdatePanel ID="Updatepanel2" runat="server">
                     <ContentTemplate>--%>
                    <div class="input-field col s12 m3 l3">
                     
                        <label>Select Roles</label><br />
                            <asp:CheckBoxList ID="chkRole" runat="server">
                                <asp:ListItem Value="ApproverFarmer" Text="ApproverFarmer" />
                                <asp:ListItem Value="ApproverVet" Text="ApproverVet" />
                                <asp:ListItem Value="ApproverParavet" Text="ApproverParavet" />
                                <asp:ListItem Value="ApproverAIT" Text="ApproverAIT" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" />
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <input type="text" id="id" class="validate" runat="server" readonly="readonly">
                      <label for="id">ID(Auto-generated)</label> 
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <input type="text" id="firstname" runat="server" class="validate">
                         <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator" ForeColor="Red" controltovalidate="firstname" errormessage="Please enter first name!" />    
                      <label for="firstname">First Name <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <input type="text" id="lastname" runat="server" class="validate">
                       <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator1" ForeColor="Red" controltovalidate="lastname" errormessage="Please enter last name!" />    
                      <label for="lastname">Last Name <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input type="email" id="email" runat="server" class="validate">
                       <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ForeColor="Red" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
                      <asp:RequiredFieldValidator runat="server" id="reqName" ForeColor="Red" controltovalidate="email" errormessage="Please enter email!" />    
                      <label for="email">Email <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input id="mobile" type="tel" runat="server" class="validate">
                      <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" ForeColor="Red" controltovalidate="mobile" errormessage="Please enter mobile!" />    
                      <label for="mobile">Mobile<span class="red-text">*</span></label>
                    </div>  
                    <div class="input-field col s12 m4 l4">
                      <input id="alternateno" type="tel" runat="server" class="validate">
                      <label for="alternateno">Alternate No</label>
                    </div>                   
                    <div class="input-field col s12 m4 l4" id="divPasss" runat="server">
                      <input type="password" id="password" runat="server" class="validate">
                           <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator3" ForeColor="Red" controltovalidate="password" errormessage="Please enter password!" />    
                      <label for="password">Password <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m4 l4" id="divConPasss" runat="server">
                      <input type="password" id="confirmpassword" runat="server" class="validate">
                       <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="confirmpassword"  ForeColor="Red" ControlToCompare="password" ErrorMessage="No Match" ToolTip="Password must be the same" />
                         <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator4" ForeColor="Red" controltovalidate="confirmpassword" errormessage="Please enter confirm password!" />    
                     <label for="confirmpassword">Confirm password <span class="red-text">*</span></label>
                    </div>
                        <%-- </ContentTemplate>
                 </asp:UpdatePanel>--%>
                  </div>
                  <div class="divider"></div>
                  <div class="step-actions modal-footer pt-1" style=" text-align: center;">
                  <button class="modal-close waves-effect waves-dark btn btn-sm btn-primary gradient-45deg-deep-orange-orange">Cancel</button>
       
                    <asp:Button ID="btnAddRoles" runat="server" class="waves-effect waves-dark btn btn-sm btn-secondary" onclick="btnAddRoles_Click"  Text="Submit"  />
                 </div>
                <%--</form>--%>
              </div>
            </div>
            <!----- Add User Modal End--->

            <!--- Edit User Modal Start-->   
           
            <!--Edit User Modal End--> 

            <!------------------------------------- ModalEnd-------------------------------------------->

            <!--Add User Floating Button Start-->
            <div class="fixed-action-btn direction-top">
              <a href="#adduserr" id="adduserrrr" class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow tooltipped modal-trigger"data-position="left" data-tooltip="Add User"><i class="material-icons">add</i></a>
            </div>
            <!--Add User Floating Button End-->
          </div>
        </div>
      </div>
    </div>
    <!-- END: Page Main-->  