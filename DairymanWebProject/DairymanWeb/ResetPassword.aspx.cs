﻿using DairymanLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DairymanWeb
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string tokenid = token.Value;
            string password = txtconpass.Value;
            string date = DateTime.Now.ToString();
            BizDataUser biz = new BizDataUser();
            lblrpError.Visible = false; lblrpSuccess.Visible = false;
            DataTable dt = biz.GetTokenDetails(tokenid, date);
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToDateTime(date) > Convert.ToDateTime(dt.Rows[0]["tokenexpireddate"].ToString()))
                {
                    lblrpError.Text = "Token Expired";
                    lblrpError.Visible = true;
                }
                else
                {
                    biz.UpdatePassword(password, dt.Rows[0]["userId"].ToString());
                    lblrpSuccess.Text = "Password Reset Successfully";
                    lblrpSuccess.Visible = true;
                }
            }
            else
            {
                lblrpError.Text = "Invalid Token";
                lblrpError.Visible = true;
            }
        }
    }
}