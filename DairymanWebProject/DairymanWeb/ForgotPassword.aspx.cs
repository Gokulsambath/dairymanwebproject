﻿using DairymanLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DairymanWeb
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string email = txtEmail.Value;
            BizDataUser biz = new BizDataUser();
            DataTable dt = biz.CheckUserEmail(email);
            lblfpSuccess.Visible = false; lblfpError.Visible = false;
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToDateTime(dt.Rows[0]["tokenexpireddate"]) > DateTime.Now)
                {
                    lblfpError.Text = "Token generation failed.please try after sometime.";
                    lblfpError.Visible = true;
                }
                else {
                    byte[] time = BitConverter.GetBytes(DateTime.Now.ToBinary());
                    byte[] key = Guid.NewGuid().ToByteArray();
                    string token = Convert.ToBase64String(time.Concat(key).ToArray());
                    DateTime tokenexpirydate = DateTime.Now;
                    DateTime tokenexpireddate = DateTime.Now.AddHours(1);
                    sendemailtoreset(token, email, tokenexpirydate, tokenexpireddate);
                    biz.Insertpasswordresettoken(dt.Rows[0]["userId"].ToString(), token, tokenexpirydate, tokenexpireddate);
                    lblfpSuccess.Text = "Check your email to reset password";
                    lblfpSuccess.Visible = true;
                }
            }
            else
            {
                lblfpError.Text = "User Not Registered in the system";
                lblfpError.Visible = true;
            }
        }

        private void sendemailtoreset(string token, string useremail, DateTime tokenexpirydate, DateTime tokenexpireddate)
        {
            string AdminEmail = ConfigurationManager.AppSettings.Get("AdminEmail");
            string Password = ConfigurationManager.AppSettings.Get("Password");
            string SMTPPort = ConfigurationManager.AppSettings.Get("SMTPPort");
            string Host = ConfigurationManager.AppSettings.Get("Host");
            string projectName = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            var verifyUrl = "/" + projectName + "/ResetPassword.aspx";
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);
            var lnkHref = "<a href='"+link+"'>Reset Password</a>";


            //HTML Template for Send email              

            string body = "<b>Please find the Password Reset Link. </b><br/>" + lnkHref;

            StringBuilder sb = new StringBuilder();
            sb.Append("Hi,<br/>Your Token is : "+token+" <br/>");
            sb.Append("<a href="+link);
            sb.Append(">Click here to change your password</a><br/>");
            sb.Append("Note: Token will get expired in next 60 Minutes.<br/><br/>");
            sb.Append("<b>Thanks</b><br/>");
            sb.Append("<b>Dairyman Admin</b><br/><br/>");
            sb.Append("<b>THIS IS SYSTEM GENERATED MAIL.PLEASE DO NOT REPLY TO THIS MAIL.<b>");

            try
            {
                MailMessage message = new System.Net.Mail.MailMessage(AdminEmail, useremail, "Dairyman - Reset Your Password", sb.ToString());

                SmtpClient smtp = new SmtpClient();

                smtp.Host = "smtp.gmail.com";

                smtp.Port =Convert.ToInt32(SMTPPort);
                

                smtp.Credentials = new System.Net.NetworkCredential(AdminEmail,Password);

                smtp.EnableSsl = true;

                message.IsBodyHtml = true;

                smtp.Send(message);

            }

            catch (Exception ex)
            {

            }


        }
    }
}