﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DairymanWeb
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Int32 userid;
            if (context.Request.QueryString["id"] != null)
                userid = Convert.ToInt32(context.Request.QueryString["id"]);
            else
                throw new ArgumentException("No parameter specified");

            context.Response.ContentType = "image/jpeg , image/jpg, image/png";
            Stream strm = GetImage(userid);
            
            byte[] buffer = new byte[4096];
           
            int byteSeq = strm.Read(buffer, 0, 4096);

            while (byteSeq > 0)
            {
                context.Response.OutputStream.Write(buffer,0, byteSeq);
                byteSeq = strm.Read(buffer, 0, 4096);
            }
        }

        public Stream GetImage(int Id)
        {
            string conn = ConfigurationManager.ConnectionStrings["dairymanDbConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(conn);
            string sql = "SELECT voterUrl FROM Userdetails WHERE Userid = @ID";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ID", Id);
            connection.Open();
            object img = cmd.ExecuteScalar();
            SqlDataReader dReader = cmd.ExecuteReader();
            dReader.Read();


            try
            {
                byte[] stringArray = Encoding.UTF8.GetBytes(img.ToString());

                return new MemoryStream(stringArray);

                //return new MemoryStream(Convert.FromBase64String(img.ToString()));

            }
            //catch
            //{
            //    //return null;
            //}
            finally
            {
                connection.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}