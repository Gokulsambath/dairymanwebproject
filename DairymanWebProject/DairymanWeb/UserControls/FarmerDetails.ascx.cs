﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DairymanLibrary.BizData;
using DairymanLibrary.Gateway;
using DairymanLibrary.Entity;
using System.Data;
using System.IO;
using System.ComponentModel;
using System.Drawing;
using System.Data.SqlClient;
using System.Configuration;

namespace DairymanWeb.UserControls
{
    public partial class FarmerDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
            {
                Response.Redirect("Login.aspx");
            }
            GetFarmerDetailsByUserid();

        }

        public void GetFarmerDetailsByUserid()
        {
            BizDataUser biz = new BizDataUser();
            string sometin = Request.QueryString["id"];
           
            if (sometin != null)
            {
                UserEntities ce = new UserEntities();
                DataSet dsFarmerById = biz.GetFarmerDetailsByUserid(sometin);
                DataTable dtFarmer = dsFarmerById.Tables[0];
                if (dsFarmerById.Tables[0].Rows.Count > 0)
                {
                    farmarid.Value = dsFarmerById.Tables[0].Rows[0]["userId"].ToString();
                    roleid.Value = (dsFarmerById.Tables[0].Rows[0]["roleId"].ToString());
                    firstname.Value = dsFarmerById.Tables[0].Rows[0]["firstName"].ToString();
                    lastname.Value = dsFarmerById.Tables[0].Rows[0]["lastName"].ToString();
                    email.Value = dsFarmerById.Tables[0].Rows[0]["email"].ToString();
                    mobile.Value = dsFarmerById.Tables[0].Rows[0]["mobileNo"].ToString();
                    alternateno.Value = dsFarmerById.Tables[0].Rows[0]["alternateNo"].ToString();
                    address.Value = dsFarmerById.Tables[0].Rows[0]["address"].ToString();
                    village.Value = dsFarmerById.Tables[0].Rows[0]["village"].ToString();
                    hamlet.Value = dsFarmerById.Tables[0].Rows[0]["hamlet"].ToString();
                    taluka.Value = dsFarmerById.Tables[0].Rows[0]["taluka"].ToString();
                    dist.Value = dsFarmerById.Tables[0].Rows[0]["district"].ToString();
                    pin.Value = dsFarmerById.Tables[0].Rows[0]["pincode"].ToString();
                    state.Value = dsFarmerById.Tables[0].Rows[0]["stateName"].ToString();
                    pan.Value = dsFarmerById.Tables[0].Rows[0]["panNo"].ToString();
                    aadhar.Value = dsFarmerById.Tables[0].Rows[0]["aadharNo"].ToString();
                    voter.Value = dsFarmerById.Tables[0].Rows[0]["voterId"].ToString();
                    commentbox.Value = dsFarmerById.Tables[0].Rows[0]["comments"].ToString();
                    //string imageUrl = "~/Handler1.ashx?id=" + sometin;
                    //string imageUrl1 = "~/Handler2.ashx?id=" + sometin;

                    //Image1.ImageUrl = Page.ResolveUrl(imageUrl);
                    //Image2.ImageUrl = imageUrl1;
                    //Image2.ImageUrl = "~/Handler1.ashx?id=" + sometin;
                    inputfilelink1.HRef = GetImageURl(sometin, "Voter",1);
                    inputfilelink2.HRef = GetImageURl(sometin, "PAN",1);
                    inputfilelink3.HRef = GetImageURl(sometin, "Aadhar",1);
                    inputfilelink4.HRef = GetImageURl(sometin, "Profile",1);
                    if (inputfilelink1.HRef == "") { divlink1.Visible = false; }
                    if (inputfilelink2.HRef == "") { divlink2.Visible = false; }
                    if (inputfilelink3.HRef == "") { divlink3.Visible = false; }
                    if (inputfilelink4.HRef == "") { divlink4.Visible = false; }
                    //inputfilenow.Value = dsFarmerById.Tables[0].Rows[0]["voterUrl"].ToString();

                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)   //Submit button approving farmers
        {
            UserEntities userentities = new UserEntities();
            Button clickedButton = sender as Button;
            
           
        
                    string sometin = Request.QueryString["id"];   //userid
                                                                  //string roleid = Request.QueryString["roleId"];  //roleid
                    userentities.userid = Convert.ToInt16(sometin);
                    
                    userentities.roleId = (Convert.ToInt32(roleid.Value));
                    userentities.firstName = firstname.Value;
                    userentities.lastName = lastname.Value;
                    userentities.Email = email.Value;
                    userentities.mobileNo = mobile.Value;
                    userentities.alternateNo = alternateno.Value;
                    userentities.address = address.Value;
                    userentities.village = village.Value;
                    userentities.hamlet = hamlet.Value;
                    userentities.taluka = taluka.Value;
                    userentities.district = dist.Value;
                    userentities.pincode = pin.Value;
                    if (state.Value == "Andhra Pradesh")
                    {
                        userentities.stateId = 1;
                    }
                    else if (state.Value == "Arunachal Pradesh")
                    {
                        userentities.stateId = 2;
                    }
                    else if (state.Value == "Assam")
                    {
                        userentities.stateId = 3;
                    }
                    else if (state.Value == "Bihar")
                    {
                        userentities.stateId = 4;
                    }
                    else if (state.Value == "Chhattisgarh")
                    {
                        userentities.stateId = 5;
                    }
                    else if (state.Value == "Delhi")
                    {
                        userentities.stateId = 6;
                    }
                    else if (state.Value == "Goa")
                    {
                        userentities.stateId = 7;
                    }
                    else if (state.Value == "Gujarat")
                    {
                        userentities.stateId = 8;
                    }
                    else if (state.Value == "Haryana")
                    {
                        userentities.stateId = 9;
                    }
                    else if (state.Value == "Himachal Pradesh")
                    {
                        userentities.stateId = 10;
                    }
                    else if (state.Value == "Jammu and Kashmir")
                    {
                        userentities.stateId = 11;
                    }
                    else if (state.Value == "Jharkhand")
                    {
                        userentities.stateId = 12;
                    }
                    else if (state.Value == "Karnataka")
                    {
                        userentities.stateId = 13;
                    }
                    else if (state.Value == "Kerala")
                    {
                        userentities.stateId = 14;
                    }
                    else if (state.Value == "Madhya Pradesh")
                    {
                        userentities.stateId = 15;
                    }
                    else if (state.Value == "Maharashtra")
                    {
                        userentities.stateId = 16;
                    }
                    else if (state.Value == "Manipur")
                    {
                        userentities.stateId = 17;
                    }
                    else if (state.Value == "Meghalaya")
                    {
                        userentities.stateId = 18;
                    }
                    else if (state.Value == "Mizoram")
                    {
                        userentities.stateId = 19;
                    }
                    else if (state.Value == "Nagaland")
                    {
                        userentities.stateId = 20;
                    }
                    else if (state.Value == "Odisha")
                    {
                        userentities.stateId = 21;
                    }
                    else if (state.Value == "Punjab")
                    {
                        userentities.stateId = 22;
                    }
                    else if (state.Value == "Rajasthan")
                    {
                        userentities.stateId = 23;

                    }
                    else if (state.Value == "Sikkim")
                    {
                        userentities.stateId = 24;
                    }

                    else if (state.Value == "Tamil Nadu")
                    {
                        userentities.stateId = 25;
                    }
                    else if (state.Value == "Telangana")
                    {
                        userentities.stateId = 26;
                    }
                    else if (state.Value == "Tripura")
                    {
                        userentities.stateId = 27;
                    }
                    else if (state.Value == "Uttar Pradesh")
                    {
                        userentities.stateId = 28;
                    }
                    else if (state.Value == "Uttarakhand")
                    {
                        userentities.stateId = 29;
                    }
                    else if (state.Value == "West Bengal")
                    {
                        userentities.stateId = 30;
                    }
                    //userentities.stateId = Convert.ToInt32(state.Value);
                    userentities.panNo = pan.Value;
                    userentities.aadharNo = aadhar.Value;
                    userentities.voterId = voter.Value;
                    //userentities.voterUrl = Image1.ImageUrl;
                    //userentities.photoUrl = Image2.ImageUrl;
                    userentities.comments = commentbox.Value;

                    BizDataUser obj = new BizDataUser();
                    obj.ApproveFarmers(userentities);
                    Response.Redirect("Data.aspx?Action=UserDetails");
                    Response.Redirect(Request.Url.AbsoluteUri);



          
        }

        protected void BtnReject_Click(object sender, EventArgs e)
        {
            UserEntities userentities = new UserEntities();
            Button clickedButton = sender as Button;

            string sometin = Request.QueryString["id"];
            userentities.userid = Convert.ToInt16(sometin);
            userentities.roleId = (Convert.ToInt32(roleid.Value));
            userentities.comments = commentbox.Value;
            BizDataUser obj = new BizDataUser();
            obj.RejectFarmers(userentities);
            Response.Redirect("Data.aspx?Action=UserDetails");

            Response.Redirect(Request.Url.AbsoluteUri);

        }
        private string GetImageURl(string sometin, string name, int role)
        {
            BizDataUser biz = new BizDataUser();
            string imageurl = biz.GetImageURL(sometin, name, role);
            return imageurl;
        }
    }
}
