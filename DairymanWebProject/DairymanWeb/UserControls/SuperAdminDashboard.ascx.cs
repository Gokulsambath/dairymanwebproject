﻿using DairymanLibrary.BizData;
using DairymanLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DairymanWeb.UserControls
{
    public partial class SuperAdminDashboard : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
            {
                Response.Redirect("Login.aspx");
            }
            BuildHTMLTable();

            if (!IsPostBack)
            {

                addedit.InnerText = "Add User";
                btnAddRoles.Text = "ADD";
                divPasss.Visible = true;
                ddlRole.Disabled = false;
                divConPasss.Visible = true;
                BizDataUser bdl = new BizDataUser();
                DataSet dsrole = bdl.GetAllConfigs();
                FillDropDown(ddlRole, dsrole.Tables[1], "Choose your option");

                EditAdminDetails();



            }

        }

        private void FillDropDown(HtmlSelect drpselect, DataTable dataTable, string v)        {            drpselect.Items.Clear();            ListItem lstItem = new ListItem();            lstItem.Value = "0";            lstItem.Text = v;            drpselect.Items.Add(lstItem);            for (int intCount1 = 0; intCount1 < dataTable.Rows.Count; intCount1++)            {                lstItem = new ListItem();                lstItem.Value = dataTable.Rows[intCount1][0].ToString().Trim();                lstItem.Text = dataTable.Rows[intCount1][1].ToString().Trim();                drpselect.Items.Add(lstItem);            }        }

        protected void btnAddRoles_Click(object sender, EventArgs e)
        {
            if (btnAddRoles.Text == "UPDATE")
            {
                UpdateUserRoles();
            }
            else
            {

                if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    UserEntities sourcelead = new UserEntities();

                    if (ddlRole.Value == "0" || firstname.Value == "" ||
                        lastname.Value == "" || email.Value == "" || mobile.Value == "" ||
                        Passs.Value == "" || ConPasss.Value == "")
                    {

                        Response.Redirect(Request.Url.AbsoluteUri);
                    }
                    if (Passs.Value != ConPasss.Value)
                    {
                        passwordError.Visible = true;
                        passwordError.Text = "Password and Confirm Password does not match";
                        Response.Redirect(Request.Url.AbsoluteUri);

                    }
                    else
                    {

                        string b = ddlRole.Value.ToString();
                        if (b == "1")
                        {
                            sourcelead.roleId = 6;
                        }
                        else if(b == "2")
                        {
                            sourcelead.roleId = 7;
                        }
                        else {
                            sourcelead.roleId = 8;
                        }
                        char ApproverFarmer = 'N', ApproverVet = 'N', ApproverParavet = 'N', ApproverAIT = 'N';
                        for (int i = 1; i <= chkRole.Items.Count; i++)
                        {

                            if (chkRole.Items[i - 1].Selected)
                            {
                                switch (i)
                                {
                                    case 1:
                                        ApproverFarmer = 'Y';
                                        break;
                                    case 2:
                                        ApproverVet = 'Y';
                                        break;
                                    case 3:
                                        ApproverParavet = 'Y';
                                        break;
                                    case 4:
                                        ApproverAIT = 'Y';
                                        break;

                                }
                            }

                        }
                        sourcelead.ApproverFarmer = ApproverFarmer;
                        sourcelead.ApproverParavet = ApproverParavet;
                        sourcelead.ApproverVet = ApproverVet;
                        sourcelead.ApproverAIT = ApproverAIT;
                        sourcelead.firstName = firstname.Value;
                        sourcelead.lastName = lastname.Value;
                        sourcelead.Email = email.Value;
                        sourcelead.mobileNo = mobile.Value;
                        sourcelead.alternateNo = alternateno.Value;
                        sourcelead.password = Passs.Value;
                        sourcelead.confirmpassword = ConPasss.Value;


                        BizDataUser obj = new BizDataUser();
                        obj.AddAdminSuperAdminApprover(sourcelead);
                        Response.Redirect("Data.aspx?Action=SuperAdminDashboard");

                        Response.Redirect(Request.Url.AbsoluteUri);

                    }
                }
            }
        }

        private void UpdateUserRoles()
        {
            if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                UserEntities sourcelead = new UserEntities();

                if (ddlRole.Value == "0" || firstname.Value == "" ||
                    lastname.Value == "" || email.Value == "" || mobile.Value == "")
                {

                    Response.Redirect(Request.Url.AbsoluteUri);
                }


                string b = ddlRole.Value.ToString();
                if (b == "1")
                {
                    sourcelead.roleId = 6;
                }
                else if(b == "2")
                {
                    sourcelead.roleId = 7;
                }
                else
                {
                    sourcelead.roleId = 8;
                }
                char ApproverFarmer = 'N', ApproverVet = 'N', ApproverParavet = 'N', ApproverAIT = 'N';
                for (int i = 1; i <= chkRole.Items.Count; i++)
                {

                    if (chkRole.Items[i - 1].Selected)
                    {
                        switch (i)
                        {
                            case 1:
                                ApproverFarmer = 'Y';
                                break;
                            case 2:
                                ApproverVet = 'Y';
                                break;
                            case 3:
                                ApproverParavet = 'Y';
                                break;
                            case 4:
                                ApproverAIT = 'Y';
                                break;

                        }
                    }

                }
                sourcelead.ApproverFarmer = ApproverFarmer;
                sourcelead.ApproverParavet = ApproverParavet;
                sourcelead.ApproverVet = ApproverVet;
                sourcelead.ApproverAIT = ApproverAIT;
                sourcelead.firstName = firstname.Value;
                sourcelead.lastName = lastname.Value;
                sourcelead.Email = email.Value;
                sourcelead.mobileNo = mobile.Value;
                sourcelead.alternateNo = alternateno.Value;
                sourcelead.password = Passs.Value;
                sourcelead.confirmpassword = ConPasss.Value;
                sourcelead.userid = Convert.ToInt32(id.Value);

                BizDataUser obj = new BizDataUser();
                obj.UpdateAdminSuperAdminApprover(sourcelead);
                Response.Redirect("Data.aspx?Action=SuperAdminDashboard");

                Response.Redirect(Request.Url.AbsoluteUri);


            }
        }

        private void BuildHTMLTable()
        {
            // throw new NotImplementedException();
            pldSuperAdminDashboard.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllDetailsForSuperAdmin();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["userId"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["roleName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["userId"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["FirstName"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["LastName"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["Email"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["MobileNo"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a href='Data.aspx?Action=SuperAdminDashboard&id=" + id + "' class='tooltipped modal-trigger' id='editus' data-position='left'  data-tooltip='Edit User'><i class='material-icons small'>edit</i></a> &nbsp;&nbsp;");
                    strAccount.Append("</td>");

                    strAccount.Append("</tr>");


                }

                pldSuperAdminDashboard.Controls.Add(new Literal { Text = strAccount.ToString() });
            }

        }
        
        private void EditAdminDetails()
        {
            BizDataUser user = new BizDataUser();
            string sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {
                UserEntities us = new UserEntities();
                DataSet dsAdminbyUserid = user.EditAdminDetails(iddd);
                if (dsAdminbyUserid.Tables[0].Rows.Count > 0)
                {
                    string role = "", ss = ""; string value;
                    role = dsAdminbyUserid.Tables[0].Rows[0]["roleid"].ToString();

                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverFarmer"].ToString() == "Y")
                    {
                        chkRole.Items.FindByValue("ApproverFarmer").Selected = true;
                    }
                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverVet"].ToString() == "Y")
                    {
                        chkRole.Items.FindByValue("ApproverVet").Selected = true;
                    }
                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverParavet"].ToString() == "Y")
                    {
                        chkRole.Items.FindByValue("ApproverParavet").Selected = true;
                    }
                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverAIT"].ToString() == "Y")
                    {
                        chkRole.Items.FindByValue("ApproverAIT").Selected = true;
                    }
                    value = role == "6" ? "1" : role == "7" ? "2" : "3" ;
                    ddlRole.Items.FindByValue(value).Selected = true;
                    id.Value = dsAdminbyUserid.Tables[0].Rows[0]["userid"].ToString();
                    firstname.Value = dsAdminbyUserid.Tables[0].Rows[0]["FirstName"].ToString();
                    lastname.Value = dsAdminbyUserid.Tables[0].Rows[0]["LastName"].ToString();
                    mobile.Value = dsAdminbyUserid.Tables[0].Rows[0]["MobileNo"].ToString();
                    email.Value = dsAdminbyUserid.Tables[0].Rows[0]["Email"].ToString();
                    alternateno.Value = dsAdminbyUserid.Tables[0].Rows[0]["AlternateNo"].ToString();
                    Passs.Value = dsAdminbyUserid.Tables[0].Rows[0]["password"].ToString();
                    ConPasss.Value = dsAdminbyUserid.Tables[0].Rows[0]["password"].ToString();
                    btnAddRoles.Text = "UPDATE";
                    if (value == "3")
                    {
                        ss = "<script language=JavaScript>$(document).ready(function(){ $('#MiddleCenterContent_ctl01_newrole').css('display', 'block'); setTimeout(function () {     document.getElementById('adduesrrrrr').click();}, 200);});</script>";
                    }
                    else
                    {
                        ss = "<script language=JavaScript>$(document).ready(function(){ $('#MiddleCenterContent_ctl01_newrole').css('display', 'none');  setTimeout(function () {   document.getElementById('adduesrrrrr').click();}, 200);});</script>";
                    }
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", ss);


                }
                ddlRole.Disabled = true;
                addedit.InnerText = "Update User";
                divPasss.Visible = false;
                divConPasss.Visible = false;
            }


        }

    }
}