﻿using DairymanLibrary.BizData;
using DairymanLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DairymanWeb.UserControls
{
    public partial class ParavetDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
            {
                Response.Redirect("Login.aspx");
            }
            GetParavetDetailsByUserid();
        }

        public void GetParavetDetailsByUserid()
        {
            BizDataUser biz = new BizDataUser();
            string sometin = Request.QueryString["id"];

            if (sometin != null)
            {
                UserEntities ce = new UserEntities();
                DataSet dsParavetById = biz.GetParavetDetailsByUserid(sometin);
                DataTable dtParavet = dsParavetById.Tables[0];
                if (dsParavetById.Tables[0].Rows.Count > 0)
                {
                    roleid.Value = dsParavetById.Tables[0].Rows[0]["roleid"].ToString();
                    paravetid.Value = dsParavetById.Tables[0].Rows[0]["userid"].ToString();
                    firstname.Value = dsParavetById.Tables[0].Rows[0]["firstName"].ToString();
                    lastname.Value = dsParavetById.Tables[0].Rows[0]["lastName"].ToString();
                    doob.Value = dsParavetById.Tables[0].Rows[0]["dob"].ToString();
                    mobile.Value = dsParavetById.Tables[0].Rows[0]["mobileNo"].ToString();
                    email.Value = dsParavetById.Tables[0].Rows[0]["email"].ToString();
                    alternateno.Value = dsParavetById.Tables[0].Rows[0]["alternateNo"].ToString();
                    address.Value = dsParavetById.Tables[0].Rows[0]["address"].ToString();
                    //string imageUrl = "~/Handler1.ashx?id=" + sometin;
                    //string imageUrl1 = "~/Handler2.ashx?id=" + sometin;
                    //string imageUrl2 = "~/Handler3.ashx?id=" + sometin;
                    //inputfile1.ImageUrl = Page.ResolveUrl(imageUrl);
                    //inputfile2.ImageUrl = imageUrl1;
                    //inputfile3.ImageUrl = imageUrl2;
                    inputfilelink1.HRef = GetImageURl(sometin, "Aadhar",4);
                    inputfilelink2.HRef = GetImageURl(sometin, "Education",4);
                    inputfilelink3.HRef = GetImageURl(sometin, "Profile",4);
                    inputfilelink4.HRef = "";
                    if (inputfilelink1.HRef == "") { divlink1.Visible = false; }
                    if (inputfilelink2.HRef == "") { divlink2.Visible = false; }
                    if (inputfilelink3.HRef == "") { divlink3.Visible = false; }
                    if (inputfilelink4.HRef == "") { divlink4.Visible = false; }
                    issueby.Value = dsParavetById.Tables[0].Rows[0]["eduAwardedBy"].ToString();
                    dob.Value = dsParavetById.Tables[0].Rows[0]["dateofCertification"].ToString();                    
                    practicingsince.Value = dsParavetById.Tables[0].Rows[0]["practisingSince"].ToString();
                    pincodescovered.Value = dsParavetById.Tables[0].Rows[0]["pincodeCovered"].ToString();
                    commentbox.Value = dsParavetById.Tables[0].Rows[0]["comments"].ToString();
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            UserEntities userentities = new UserEntities();
            Button clickedButton = sender as Button;
            string sometin = Request.QueryString["id"];
            userentities.userid = Convert.ToInt16(sometin);

            userentities.roleId = (Convert.ToInt32(roleid.Value));
            BizDataUser obj = new BizDataUser();
            userentities.comments = commentbox.Value;
            obj.ApproveFarmers(userentities);

            Response.Redirect(Request.Url.AbsoluteUri);
        }

        protected void BtnReject_Click(object sender, EventArgs e)
        {
            UserEntities userentities = new UserEntities();
            Button clickedButton = sender as Button;

            string sometin = Request.QueryString["id"];
            userentities.userid = Convert.ToInt16(sometin);
            userentities.roleId = (Convert.ToInt32(roleid.Value));
            userentities.comments = commentbox.Value;
            BizDataUser obj = new BizDataUser();
            obj.RejectFarmers(userentities);
            Response.Redirect("Data.aspx?Action=UserDetails");

            //Response.Redirect(Request.Url.AbsoluteUri);
        }
        private string GetImageURl(string sometin, string name, int role)
        {
            BizDataUser biz = new BizDataUser();
            string imageurl = biz.GetImageURL(sometin, name, role);
            return imageurl;
        }
    }
}