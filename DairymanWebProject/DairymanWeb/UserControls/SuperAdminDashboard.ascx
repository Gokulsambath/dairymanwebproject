﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperAdminDashboard.ascx.cs" Inherits="DairymanWeb.UserControls.SuperAdminDashboard" %>

    
<style>
    [type='checkbox']:not(:checked), [type='checkbox']:checked{
        opacity:1 !important;
        position:relative!important;
    }
   
</style>
<script>
    function newroles() {
        console.log($("#MiddleCenterContent_ctl01_ddlRole option:selected").val());
        if ($("#MiddleCenterContent_ctl01_ddlRole option:selected").val() == 3) {
            $("#MiddleCenterContent_ctl01_newrole").css("display", "block");
        } else {
             $("#MiddleCenterContent_ctl01_newrole").css("display", "none");
        }
    }
</script>
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">Super Admin Dashboard</h5>
              </div>
            </div>
          </div>
        </div>

        <!----------------------------------------- table start ---------------------------------->
        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">
  
                <!-- Page Length Options -->
                <div class="row animate fadeUp">
                  <div class="col s12">
                    <div class="card">
                      <div class="card-content" style="">
                        <div class="row">
                          <div class="col s12">
                            <table id="page-length-option" class="display striped centered" style="width:100%">
                              <thead>
                                <tr>
                                  <th>Role</th>
                                  <th>ID</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Email</th>
                                  <th>Mobile</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                              <asp:PlaceHolder ID="pldSuperAdminDashboard" runat="server"></asp:PlaceHolder>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

</div><!-- -------------------------------table end ---------------------------------------->

<!---------------------------All  Modal ------------------------------>
          <!----- Add User Modal Start--->
            <div id="adduser" class="modal">
                <div class="modal-content pt-0">
                <span class="modal-header right modal-close">
                  <i class="material-icons right-align">clear</i>
                </span>
                <h5 style="font-size:22px;text-align:left; color: #5c6bc0;">
                  <strong><span id="addedit" runat="server">Add User</span></strong>
                </h5>
                <div class="divider"></div>
                 
                  <div class="row">
                       
                     <div class="input-field col s12 m3 l3" id="newrole" runat="server" style="display:none">
                     
                        <label>Select Roles</label><br />
                            <asp:CheckBoxList ID="chkRole" runat="server">
                                <asp:ListItem Value="ApproverFarmer" Text="ApproverFarmer" />
                                <asp:ListItem Value="ApproverVet" Text="ApproverVet" />
                                <asp:ListItem Value="ApproverParavet" Text="ApproverParavet" />
                                <asp:ListItem Value="ApproverAIT" Text="ApproverAIT" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" />
                    </div>
                    <div class="input-field col s12 m3 l3">
                        <select class="validate" id="ddlRole" onchange="newroles()" runat="server">
                         
                        </select>  
                        
                        <label>Role <span class="red-text">*</span></label>
                        <asp:RequiredFieldValidator ID="reqyear"
                                        ControlToValidate="ddlRole"
                                        ErrorMessage="Please Select" InitialValue="0" Text="Please select" ForeColor="Red"
                                        runat="server" />
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <input type="text" id="id" class="validate" runat="server" readonly="readonly">
                      <label for="id">ID(Auto-generated)</label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <input type="text" id="firstname" class="validate"  runat="server">
                        <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator" ForeColor="Red" controltovalidate="firstname" errormessage="Please enter first name!" />    
                      <label for="firstname">First Name <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <input type="text" id="lastname" class="validate"  runat="server">
                         <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator1" ForeColor="Red" controltovalidate="lastname" errormessage="Please enter last name!" />    
                      <label for="lastname">Last Name <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input type="email" id="email" class="validate"  runat="server">
                        <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ForeColor="Red" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
                         <asp:RequiredFieldValidator runat="server" id="reqName" ForeColor="Red" controltovalidate="email" errormessage="Please enter email!" />    
                      <label for="email">Email <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input id="mobile" type="tel" class="validate"  runat="server">
                         <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" ForeColor="Red" controltovalidate="mobile" errormessage="Please enter mobile!" />    
                      <label for="mobile">Mobile<span class="red-text">*</span></label>
                    </div>  
                    <div class="input-field col s12 m4 l4">
                      <input id="alternateno" type="tel" class="validate"  runat="server">
                      <label for="alternateno">Alternate No</label>
                    </div>                   
                    <div class="input-field col s12 m6 l6" id="divPasss" runat="server">
                      <input type="password" id="Passs" class="validate"  runat="server">
                         <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator3" ForeColor="Red" controltovalidate="Passs" errormessage="Please enter password!" />    
                      <label for="Passs">Password <span class="red-text">*</span></label>
                    </div>
                    <div class="input-field col s12 m6 l6" id="divConPasss" runat="server">
                      <input type="password" id="ConPasss" class="validate"  runat="server">
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ConPasss"  ForeColor="Red" ControlToCompare="Passs" ErrorMessage="No Match" ToolTip="Password must be the same" />
                         <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator4" ForeColor="Red" controltovalidate="ConPasss" errormessage="Please enter confirm password!" />    
                      <label for="ConPasss">Confirm password <span class="red-text">*</span></label>
                    </div>
                      <div class="col-md-12">
                            <asp:Label ID="passwordError" runat="server" CssClass="danger" Visible="false"></asp:Label>
                        </div>
                       
                  </div>
                  <div class="divider"></div>
                  <div class="step-actions modal-footer pt-1" style=" text-align: center;">
                    <button class="modal-close waves-effect waves-dark btn btn-sm btn-primary gradient-45deg-deep-orange-orange">Cancel</button>
       
                    <asp:Button ID="btnAddRoles" runat="server" class="waves-effect waves-dark btn btn-sm btn-secondary" onclick="btnAddRoles_Click"  Text="Submit"  />

                  </div>
                
              </div> 
            </div>
          
            <div class="fixed-action-btn direction-top">
              <a href="#adduser" id="adduesrrrrr" class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow tooltipped modal-trigger"data-position="left" data-tooltip="Add User"><i class="material-icons">add</i></a>
            </div>
            <!--Add User Floating Button End-->
          </div>
        </div>
      </div>
    </div>
    <!-- END: Page Main--> 