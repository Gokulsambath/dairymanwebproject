﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs" Inherits="DairymanWeb.UserControls.UserDetails" %>
<style>
    td{
        white-space:nowrap !important;
        /*white-space:normal !important;*/
        max-width:10px !important;
        word-break:break-all !important;
        overflow: hidden !important;
        text-overflow: ellipsis !important;
    }
    td:hover{
        overflow: visible !important; 
        white-space: normal !important;        
    }
    #main .section-data-tables .dataTables_wrapper .dataTables_length, #main .section-data-tables .dataTables_wrapper .dataTables_filter{
        display:block !important;
    }
</style>
  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>

        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper"><!-- Page Name Start-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">User Details</h5>
              </div>
            </div>
          </div>
        </div><!-- Page Name End-->

        <div class="col s12">
          <div class="container">
            <div class="row">
              <div class="col m12 animate fadeUp">

                <!-- Tabs Start-->
                <div id="tabs-in-card" class="section">
                  <div class="row">
                    <div class="col  s12 m12 l12">
                      <div class="card">
                        <div class="card-tabs">
                          <ul class="tabs tabs-fixed-width">
                            <li class="tab">
                              <a class="active" id="farmerlabel" runat="server" href="#farmer"></a>
                            </li>
<!--                             <li class="tab">
                              <a  href="#farmhelper">Farm Helper</a>
                            </li> -->
                            <li class="tab">
                              <a  href="#vet" id="Vetlabel" runat="server"></a>
                            </li>
                            <li class="tab">
                              <a href="#paravet" id="Paravetlabel" runat="server"></a>
                            </li> 
                            <li class="tab">
                              <a href="#ait" id="AITlabel" runat="server"></a>
                            </li> 
                          </ul>
                        </div>
                        <div class="card-content grey lighten-4"><!--- Card Content  Start-->

                        <div id="farmer"><!--- Farmer Tab Start-->
                        <!------------------------------ table start ---------------------------------->
                        <div class="section section-data-tables">
                          <!-- Page Length Options -->
                          <div class="row">
                            <div class="col s12">
                              <div class="card">
                                <div class="card-content" style="">
                                  <div class="row">
                                    <div class="col s12">
                                      <table id="datatable" class="display striped centered" style="width:100%;">
                                        <thead>
                                          <tr>
                                            <th>Farmer ID</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Status</th>
                                            <th>Pin</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          <asp:PlaceHolder ID="pldFarmer" runat="server"></asp:PlaceHolder>
                                              <asp:HiddenField ID="roleId" runat="server" />  

                                     
                                        </tbody>
                                      </table>
                                       <!--  Add Customer Floating Button Start
                                      <div class="right-align" style="bottom:10px; right: 19px; margin-bottom: 10px;"  class="fixed-action-btn  direction-top"><a href="#addlocation" class="btn-floating  gradient-45deg-light-blue-cyan gradient-shadow tooltipped modal-trigger"    data-position="left" data-tooltip="Add Location"><i class="material-icons">add</i></a>
                                     </div> Add Customer Floating Button End -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>  
                        </div><!-------table end ------------>
                      </div><!--- Farmer Tab End-->

                      <div id="vet"><!--- Vet Tab Start-->
                        <!--------------------------------- table start ---------------------------------->
                        <div class="section section-data-tables">
                          <!-- Page Length Options -->
                          <div class="row">
                            <div class="col s12">
                              <div class="card">
                                <div class="card-content" style="">
                                  <div class="row">
                                    <div class="col s12 ">
                                      <table id="datatable2" class="display striped centered" style="width:100%;">
                                        <thead>
                                          <tr>
                                            <th>Vet ID</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Address</th>
                                            <th>Practice License No</th>
                                            <th>Status</th>
                                            
                                            
                                          </tr>
                                          </thead>
                                          <tbody>
                                           <asp:PlaceHolder ID="pldVet" runat="server"></asp:PlaceHolder>

                                       </tbody>
                                      </table>
                                       <!--  Add Customer Floating Button Start
                                      <div class="right-align" style="bottom:10px; right: 19px; margin-bottom: 10px;"  class="fixed-action-btn  direction-top"><a href="#addlocation" class="btn-floating  gradient-45deg-light-blue-cyan gradient-shadow tooltipped modal-trigger"    data-position="left" data-tooltip="Add Location"><i class="material-icons">add</i></a>
                                     </div> Add Customer Floating Button End -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>  
                        </div><!-------table end ------------>
                      </div><!--- Vet Tab End-->

                      <div id="paravet"><!--- Paravet Tab Start-->
                      <!------------------------- table start ---------------------------------->
                        <div class="section section-data-tables">
                          <!-- Page Length Options -->
                          <div class="row">
                            <div class="col s12">
                              <div class="card">
                                <div class="card-content" style="">
                                  <div class="row">
                                    <div class="col s12 ">
                                      <table id="datatable3" class="display striped centered" style="width:100%;">
                                        <thead>
                                          <tr>
                                            <th>Paravet ID</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Address</th>
                                            <th>Practice License No</th>
                                            <th>Status</th>
                                            
                                            
                                          </tr>
                                          </thead>
                                          <tbody>

                                         <asp:PlaceHolder ID="pldParavet" runat="server"></asp:PlaceHolder>

                                       
                                        </tbody>
                                      </table>
                                       <!--  Add Customer Floating Button Start
                                      <div class="right-align" style="bottom:10px; right: 19px; margin-bottom: 10px;"  class="fixed-action-btn  direction-top"><a href="#addlocation" class="btn-floating  gradient-45deg-light-blue-cyan gradient-shadow tooltipped modal-trigger"    data-position="left" data-tooltip="Add Location"><i class="material-icons">add</i></a>
                                     </div> Add Customer Floating Button End -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>  
                        </div><!-------table end ------------>
                      </div><!--- Paravet Tab End-->

                      <div id="ait"><!--- AIT Tab Start-->
                        <!------------------------------------- table start ---------------------------------->
                        <div class="section section-data-tables">
                          <!-- Page Length Options -->
                          <div class="row">
                            <div class="col s12">
                              <div class="card">
                                <div class="card-content" style="">
                                  <div class="row">
                                    <div class="col s12 ">
                                      <table id="datatable4" class="display striped centered" style="width:100%;">
                                        <thead>
                                          <tr>
                                            <th>AIT ID</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Address</th>
                                            <th>Practicing Since</th>
                                            <th>Status</th>
                                            
                                            
                                          </tr>
                                          </thead>
                                          <tbody>

                                          <asp:PlaceHolder ID="pldAI" runat="server"></asp:PlaceHolder>

                                       
                                        </tbody>
                                      </table>
                                       <!--  Add Customer Floating Button Start
                                      <div class="right-align" style="bottom:10px; right: 19px; margin-bottom: 10px;"  class="fixed-action-btn  direction-top"><a href="#addlocation" class="btn-floating  gradient-45deg-light-blue-cyan gradient-shadow tooltipped modal-trigger"    data-position="left" data-tooltip="Add Location"><i class="material-icons">add</i></a>
                                     </div> Add Customer Floating Button End -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>  
                        </div><!-------table end ------------>
                      </div><!--- AIT Tab End-->

                    </div><!--- Card Content  End-->
                  </div><!-- Card End--->
                </div><!-- Column End--->
              </div><!-- Row End---->
            </div><!--  Tabs in Cards End-->
                
        </div><!--- Main Column End-->        
    </div><!--- Main Row End-->


          </div>
        </div>
      </div>
    </div>
    <!-- END: Page Main-->


<script>
  
    $('table.display').dataTable();
</script>