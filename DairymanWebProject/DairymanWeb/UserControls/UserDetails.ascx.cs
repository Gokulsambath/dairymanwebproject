﻿using DairymanLibrary.BizData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DairymanWeb.UserControls
{
    public partial class UserDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
            {
                Response.Redirect("Login.aspx");
            }
            BuildHTMLTable();
            BuildHTMLTable1();
            BuildHTMLTable2();
            BuildHTMLTable3();



        }

        private void BuildHTMLTable()   //Farmer
        {
            // throw new NotImplementedException();
            pldFarmer.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllFarmerDetails();
            StringBuilder strAccount = new StringBuilder();
            farmerlabel.InnerText = "FARMER (" + dsUser.Tables[0].Rows.Count + ")";
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["userid"].ToString();
                    roleId.Value = dsUser.Tables[0].Rows[intCount]["roleId"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    //strAccount.Append(dsUser.Tables[0].Rows[intCount]["userid"].ToString());
                    strAccount.Append("<a href = 'Data.aspx?Action=FarmerDetails&id=" + id + "' >" + dsUser.Tables[0].Rows[intCount]["userid"].ToString()+ "</a>");

                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["FullName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["mobileNo"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["district"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["stateName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["Status"].ToString() == "Y" ? "Approved" : dsUser.Tables[0].Rows[intCount]["Status"].ToString() == "R" ? "Rejected" : "Not Approved");
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["pincode"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("</tr>");
                    
                  

                }

                pldFarmer.Controls.Add(new Literal { Text = strAccount.ToString() });
            }

        }


        private void BuildHTMLTable1()     //Vet
        {
            // throw new NotImplementedException();
            pldVet.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllVetDetails();
            StringBuilder strAccount = new StringBuilder();
            Vetlabel.InnerText = "VET ("+ dsUser.Tables[0].Rows.Count + ")";
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["userid"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    //strAccount.Append(dsUser.Tables[0].Rows[intCount]["userid"].ToString());
                    strAccount.Append("<a href = 'Data.aspx?Action=VetDetails&id=" + id + "' >" + dsUser.Tables[0].Rows[intCount]["userid"].ToString() + "</a>");

                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["FullName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["mobileNo"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["address"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["practiseLicenseNo"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["Status"].ToString() == "Y" ? "Approved" : dsUser.Tables[0].Rows[intCount]["Status"].ToString() == "R" ? "Rejected" : "Not Approved");
                    strAccount.Append("</td>");
                     strAccount.Append("</tr>");
                   

                }

                pldVet.Controls.Add(new Literal { Text = strAccount.ToString() });
            }

        }

        private void BuildHTMLTable2()     //Paravet
        {
            // throw new NotImplementedException();
            pldParavet.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllParavetDetails();
            StringBuilder strAccount = new StringBuilder();
            Paravetlabel.InnerText = "PARAVET ("+ dsUser.Tables[0].Rows.Count + ")";
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["userid"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    //strAccount.Append(dsUser.Tables[0].Rows[intCount]["userid"].ToString());
                    strAccount.Append("<a href = 'Data.aspx?Action=ParavetDetails&id=" + id + "' >" + dsUser.Tables[0].Rows[intCount]["userid"].ToString() + "</a>");

                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["FullName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["mobileNo"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["address"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["practiseLicenseNo"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["Status"].ToString() == "Y" ? "Approved" : dsUser.Tables[0].Rows[intCount]["Status"].ToString() == "R" ? "Rejected" : "Not Approved");
                    strAccount.Append("</td>");
                    strAccount.Append("</tr>");
                   

                }

                pldParavet.Controls.Add(new Literal { Text = strAccount.ToString() });
            }

        }


        private void BuildHTMLTable3()     //AI
        {
            // throw new NotImplementedException();
            pldAI.Controls.Clear();
            BizDataUser objBizDataUser = new BizDataUser();
            DataSet dsUser = objBizDataUser.GetAllAIDetails();
            StringBuilder strAccount = new StringBuilder();
            AITlabel.InnerText = "AIT("+ dsUser.Tables[0].Rows.Count +")";
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["userid"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    //strAccount.Append(dsUser.Tables[0].Rows[intCount]["userid"].ToString());
                    strAccount.Append("<a href = 'Data.aspx?Action=AIDetails&id=" + id + "' >" + dsUser.Tables[0].Rows[intCount]["userid"].ToString() + "</a>");

                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["FullName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["mobileNo"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["address"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["practisingSince"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["Status"].ToString() =="Y" ? "Approved" : dsUser.Tables[0].Rows[intCount]["Status"].ToString() == "R" ? "Rejected" : "Not Approved");
                    strAccount.Append("</td>");
                    strAccount.Append("</tr>");
                    

                }

                pldAI.Controls.Add(new Literal { Text = strAccount.ToString() });
            }

        }



    }
}