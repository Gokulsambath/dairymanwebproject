﻿using DairymanLibrary.BizData;
using DairymanLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DairymanWeb.UserControls
{
    public partial class ApproverDashboard : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
            {
                Response.Redirect("Login.aspx");
            }
            BuildHTMLTable();

            if (!IsPostBack)
            {
                addedit.InnerText = "Add User";
                btnAddRoles.Text = "ADD";
                divPasss.Visible = true;
                
                
                divConPasss.Visible = true;
                BizDataLookup bdl = new BizDataLookup();
              
                EditApproveDetails();



            }

        }

       
        private void BuildHTMLTable()
        {
            pldApproverDashboard.Controls.Clear();
            BizDataApprover objBizDataUser = new BizDataApprover();
            DataSet dsUser = objBizDataUser.GetAllDetailsApprover();


            StringBuilder strAccount = new StringBuilder();
            if (dsUser.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < dsUser.Tables[0].Rows.Count; intCount++)
                {
                    string id = dsUser.Tables[0].Rows[intCount]["userId"].ToString();
                    strAccount.Append("<tr>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["roleName"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["userId"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["FirstName"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["LastName"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["Email"].ToString());
                    strAccount.Append("</td>");

                    strAccount.Append("<td>");
                    strAccount.Append(dsUser.Tables[0].Rows[intCount]["MobileNo"].ToString());
                    strAccount.Append("</td>");
                    strAccount.Append("<td class='center-align'>");
                    strAccount.Append("<a href='Data.aspx?Action=ApproverDashboard&id=" + id + "' class='tooltipped modal-trigger' id='editus' data-position='left'  data-tooltip='Edit User'><i class='material-icons small'>edit</i></a> &nbsp;&nbsp;");
                    strAccount.Append("</td>");

                    strAccount.Append("</tr>");


                }

                pldApproverDashboard.Controls.Add(new Literal { Text = strAccount.ToString() });
            }

        }

        protected void btnAddRoles_Click(object sender, EventArgs e)
        {
            if (btnAddRoles.Text == "UPDATE")
            {
                UpdateApproverRoles();
            }
            else
            {

                if (Session["UserID"].ToString().Equals("0") || Session["UserID"].ToString().Equals(""))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    UserEntities sourcelead = new UserEntities();
                    char ApproverFarmer='N', ApproverVet= 'N', ApproverParavet= 'N', ApproverAIT= 'N';
                    for (int i = 1; i <= chkRole.Items.Count; i++)
                    {

                        if (chkRole.Items[i-1].Selected)
                        {
                            switch (i)
                            {
                                case 1:
                                    ApproverFarmer = 'Y';
                                    break;
                                case 2:
                                    ApproverVet = 'Y';
                                    break;
                                case 3:
                                    ApproverParavet = 'Y';
                                    break;
                                case 4:
                                    ApproverAIT = 'Y';
                                    break;

                            }
                        }

                    }
                    sourcelead.roleId = 8;
                    sourcelead.ApproverFarmer = ApproverFarmer;
                    sourcelead.ApproverParavet = ApproverParavet;
                    sourcelead.ApproverVet = ApproverVet;
                    sourcelead.ApproverAIT = ApproverAIT;
                    sourcelead.firstName = firstname.Value;
                    sourcelead.lastName = lastname.Value;
                    sourcelead.Email = email.Value;
                    sourcelead.mobileNo = mobile.Value;
                    sourcelead.alternateNo = alternateno.Value;
                    sourcelead.password = password.Value;
                    sourcelead.confirmpassword = confirmpassword.Value;


                    BizDataApprover obj = new BizDataApprover();
                    obj.AddApprover(sourcelead);
                    Response.Redirect("Data.aspx?Action=ApproverDashboard");

                    Response.Redirect(Request.Url.AbsoluteUri);

                    
                }
            }
        }

        public void EditApproveDetails()
        {
            BizDataUser user = new BizDataUser();
            string sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);

            if (sometin != null)
            {
                UserEntities us = new UserEntities();
                DataSet dsAdminbyUserid = user.EditAdminDetails(iddd);
                if (dsAdminbyUserid.Tables[0].Rows.Count > 0)
                {
                   
                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverFarmer"].ToString() == "Y") {
                        chkRole.Items.FindByValue("ApproverFarmer").Selected = true;
                    }
                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverVet"].ToString() == "Y") {
                        chkRole.Items.FindByValue("ApproverVet").Selected = true;
                    }
                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverParavet"].ToString() == "Y")
                    {
                        chkRole.Items.FindByValue("ApproverParavet").Selected = true;
                    }
                    if (dsAdminbyUserid.Tables[0].Rows[0]["ApproverAIT"].ToString() == "Y") {
                        chkRole.Items.FindByValue("ApproverAIT").Selected = true;
                    }
                    id.Value = dsAdminbyUserid.Tables[0].Rows[0]["userid"].ToString();
                    firstname.Value = dsAdminbyUserid.Tables[0].Rows[0]["FirstName"].ToString();
                    lastname.Value = dsAdminbyUserid.Tables[0].Rows[0]["LastName"].ToString();
                    mobile.Value = dsAdminbyUserid.Tables[0].Rows[0]["MobileNo"].ToString();
                    email.Value = dsAdminbyUserid.Tables[0].Rows[0]["Email"].ToString();
                    alternateno.Value = dsAdminbyUserid.Tables[0].Rows[0]["AlternateNo"].ToString();
                    password.Value = dsAdminbyUserid.Tables[0].Rows[0]["password"].ToString();
                    confirmpassword.Value = dsAdminbyUserid.Tables[0].Rows[0]["password"].ToString();
                    btnAddRoles.Text = "UPDATE";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CreatePopupOpen", "<script language=JavaScript>$(document).ready(function(){ setTimeout(function () {     document.getElementById('adduserrrr').click();}, 200);});</script>");
                   


                }
                addedit.InnerText = "Update User";
                divPasss.Visible = false;
                divConPasss.Visible = false;
            }

        }


        public void UpdateApproverRoles()
        {
            UserEntities sourcelead = new UserEntities();
            string sometin = Request.QueryString["id"];
            int iddd = Convert.ToInt32(sometin);
            sourcelead.userid = iddd;
            char ApproverFarmer = 'N', ApproverVet = 'N', ApproverParavet = 'N', ApproverAIT = 'N';
            for (int i = 1; i <= chkRole.Items.Count; i++)
            {

                if (chkRole.Items[i - 1].Selected)
                {
                    switch (i)
                    {
                        case 1:
                            ApproverFarmer = 'Y';
                            break;
                        case 2:
                            ApproverVet = 'Y';
                            break;
                        case 3:
                            ApproverParavet = 'Y';
                            break;
                        case 4:
                            ApproverAIT = 'Y';
                            break;

                    }
                }

            }
            sourcelead.roleId = 8;
            sourcelead.ApproverFarmer = ApproverFarmer;
            sourcelead.ApproverParavet = ApproverParavet;
            sourcelead.ApproverVet = ApproverVet;
            sourcelead.ApproverAIT = ApproverAIT;
            sourcelead.firstName = firstname.Value;
            sourcelead.lastName = lastname.Value;
            sourcelead.Email = email.Value;
            sourcelead.mobileNo = mobile.Value;
            sourcelead.alternateNo = alternateno.Value;


            BizDataApprover obj = new BizDataApprover();
            obj.UpdateApprover(sourcelead);
            Response.Redirect("Data.aspx?Action=ApproverDashboard");

            Response.Redirect(Request.Url.AbsoluteUri);


        }
    }
}