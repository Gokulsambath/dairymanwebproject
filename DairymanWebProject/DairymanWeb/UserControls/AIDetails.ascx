﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AIDetails.ascx.cs" Inherits="DairymanWeb.UserControls.AIDetails" %>



    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0">AIT Details</h5>
              </div>     
            </div>
          </div>
        </div>

        <div class="col s12">
          <div class="container">
            <div class="section section-data-tables">


        <div class="row animate fadeUp">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
                  <%--<form  novalidate="novalidate">--%>
                    <div class="input-field col x12 s12 m3 l3">
                      <input type="text" id="aitid" runat="server"  class="validate">
                      <label for="vetid">AIT ID</label>
                        <input type="hidden" id="roleid" runat="server" />
                    </div>
                    <div class="input-field col x12 s12 m3 l3">
                      <input type="text" id="firstname" runat="server"  class="validate">
                      <label for="firstname">Firstname</label>
                    </div>
                    <div class="input-field col x12 s12 m3 l3">
                      <input type="text" id="lastname" runat="server"  class="validate">
                      <label for="lastname">Lastname</label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <label for="dob">Date of Birth</label>
                      <input type="text" class="datepicker" id="doob" runat="server"  placeholder="DD-MM-YYYY">
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="text" id="email" runat="server"  class="validate">
                      <label for="email">Email</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="tel" id="mobile" runat="server"  class="validate">
                      <label for="mobile">Mobile</label>
                    </div>
                    <div class="input-field col x12 s12 m4 l4">
                      <input type="tel" id="alternateno" runat="server"  class="validate">
                      <label for="alternateno">Alternate No</label>
                    </div>
                    <div class="input-field col s12">
                      <textarea id="address" runat="server"  class="materialize-textarea"></textarea>
                      <label for="address">Address</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                      <select id="option" runat="server" >
                        <option value="Select" disabled selected>Choose your option</option>
                        <option value="voterid">Voter ID</option>
                        <option value="aadhar">Aadhar</option>
                        <option value="panid">PAN ID</option>                      
                      </select>
                      <label>Select your ID Proof</label>
                    </div>
                    <div class="input-field col x12 s12 m6 l6">
                      <input type="text" id="idnumber" runat="server"  class="validate">
                      <label for="idnumber">Enter your ID number</label>
                    </div>
                 
                    <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink1" runat="server">
                          <div class="row">
                            <div class="col s12 m4 l3">
                                <p>Upload Aadhar</p>
                              </div>
                              <div class="col s12 m8 l9">
                            
                                   <a runat="server" id="inputfilelink1" target="_blank">View Photo</a>
                              </div>
                            </div>

                         </div> 
                    <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink2" runat="server">
                          <div class="row">
                            <div class="col s12 m4 l3">
                                  <p>Upload Education Degree</p>
                              </div>
                              <div class="col s12 m8 l9">
                            
                                  <a runat="server" id="inputfilelink2" target="_blank">View Photo</a>
                              </div>
                            </div>

                         </div> 
                    <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink3" runat="server">
                      <div class="row">
                        <div class="col s12 m4 l3">
                            <p>Upload Profile Photo</p>
                        </div>
                        <div class="col s12 m8 l9">
                          <a runat="server" ID="inputfilelink3" target="_blank">View Photo</a>
                        </div> 
                      </div>
                    </div>
                   <div class="col x12 s12 m12 l12 mt-2 mb-2" id="divlink4" runat="server">
                      <div class="row">
                        <div class="col s12 m4 l3">
                            <p>Upload Education (Vet Degree)</p>
                        </div>
                        <div class="col s12 m8 l9">
                          <a runat="server" ID="inputfilelink4" target="_blank">View Photo</a>
                        </div> 
                      </div>
                    </div>

                    <div class="input-field col x12 s12 m6 l6">
                      <input type="text" id="issueby"  runat="server" class="validate">
                      <label for="issueby">Issued By (Institute Name)</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                      <label for="dob">Date of Certification</label>
                      <input type="text" class="datepicker"  runat="server" id="dob" placeholder="DD-MM-YYYY">
                    </div>
                    <div class="input-field col x12 s12 m6 l6">
                      <input type="text" id="practicingsince"  runat="server" class="validate">
                      <label for="practicingsince">Practicing Since</label>
                    </div>
                    <div class="input-field col x12 s12 m6 l6">
                      <input type="text" id="pincodescovered"  runat="server" class="validate">
                      <label for="pincodescovered">Pin code covered (Where he can provide service)</label>
                    </div>
                    
                    <div class="col x12 s12 m12 l12"  id="showcomment">
                      <div class="divider"></div>
                      <div class="input-field col s12">
                        <textarea id="commentbox" class="materialize-textarea" runat="server"></textarea>
                        <label for="">Comment Box</label>
                      </div>
                      <div class="step-actions modal-footer pt-2 mb-2" style="text-align: center;">
                       
                   <asp:Button ID="btnSubmit" runat="server" class="btn btn-sm btn-primary gradient-45deg-green-teal" onclick="btnSubmit_Click"  Text="Approve"  />
                     
                    <asp:Button ID="BtnReject" runat="server" class="btn btn-sm  gradient-45deg-deep-orange-orange" onclick="BtnReject_Click"  Text="Reject"  />
                                    
                      </div>
                    </div> 
                  <%--</form>--%>
              </div>
            </div>
          </div>
        </div>
      </div>

          </div>
        </div>
      </div>
    </div>
    <!-- END: Page Main-->

    