﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalHeader.ascx.cs" Inherits="DairymanWeb.UserControls.GlobalHeader" %>
 <!-- BEGIN: Header-->
  <header class="page-topbar" id="header">
     <div class="navbar navbar-fixed"> 
       <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark  no-shadow" style="background: white">
         <div class="nav-wrapper">
          
           <ul class="navbar-list right">
             <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons" style="color: black">settings_overscan</i></a></li>
             <%--<li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons" style="color: black">notifications_none<small class="notification-badge">5</small></i></a></li>--%>
             <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><%--<img src="app-assets/images/login-page-logo-img.png" alt="avatar">--%><i></i></span></a></li>
           </ul>
           <!-- notifications-dropdown Start-->
          <%-- <ul class="dropdown-content" id="notifications-dropdown">
             <li>
               <h6>NOTIFICATIONS<span class="new badge">5</span></h6>
             </li>
             <li class="divider"></li>
             <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> A new order has been placed!</a>
               <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
             </li>
             <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle red small">stars</span> Completed the task</a>
               <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
             </li>
             <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle teal small">settings</span> Settings updated</a>
               <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
             </li>
             <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle deep-orange small">today</span> Director meeting started</a>
               <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
             </li>
             <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle amber small">trending_up</span> Generate monthly report</a>
               <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
             </li>
           </ul>--%>
           <!-- notifications-dropdown End-->
           <!-- profile-dropdown Start-->
           <ul class="dropdown-content" id="profile-dropdown">
               <%-- <li><a class="grey-text text-darken-1" href="user-profile-page.html"><i class="material-icons">person_outline</i> Profile</a></li>
                <li><a class="grey-text text-darken-1 modal-trigger" href="#changepassword"><i class="material-icons">chat_bubble_outline</i>Change Password</a></li>
                <li><a class="grey-text text-darken-1" href="page-faq.html"><i class="material-icons">help_outline</i> Help</a></li>
                <li class="divider"></li>
                <li><a class="grey-text text-darken-1" href="user-lock-screen.html"><i class="material-icons">lock_outline</i> Lock</a></li>--%>
                <li><a class="grey-text text-darken-1" href="Login.aspx"><i class="material-icons">keyboard_tab</i> Logout</a></li>
            </ul>
            <!-- profile-dropdown End-->
            <!-- Change Password Modal Start -->
             <div id="changepassword" class="modal fade in changepassword"  role="dialog" data-keyboard="false" data-backdrop="static">
                <div class="modal-content pt-0">
                  <span class="modal-header right modal-close cross"><i class="material-icons right-align" style="color:grey; position: absolute;top: -8px; right:5px;">clear</i></span>
                    <h5 style="font-size:22px;text-align:left; color: #5c6bc0;"><strong> Change Password</strong></h5>
                      <div class="divider"></div>
                        <form action="" method="">
                         <div class="row">
                            <div class="input-field col s12 m12 l12">
                              <input type="password" id="oldpassword" class="validate" required>
                              <label for="oldpassword" style="margin-bottom: 1px;">Old Password <span class="red-text">*</span></label>
                            </div>
                            <div class="input-field col s12 m12 l12">
                              <input type="password" id="newpassword" class="validate" required>
                              <label for="newpassword">New Password <span class="red-text">*</span></label>
                            </div>
                            <div class="input-field col s12 m12 l12">
                              <input type="password" id="confirmpassword" class="validate" required>
                              <label for="confirmpassword">Confirm Password <span class="red-text">*</span></label>
                            </div>
                          </div>
                          <dir class="divider"></dir>
                          <div class="step-actions modal-footer" style="text-align:center; margin-top: -10px;">
                          <button class="waves-effect waves-dark btn btn-sm btn-primary modal-close gradient-45deg-deep-orange-orange">Cancel</button>
                          <button class="waves-effect waves-dark btn btn-sm btn-secondary" type="submit" name="action">Submit</button>
                          </div>
                        </form>
                    </div>
                 </div>
                 <!-- Change Password Modal End -->
             </div>
           </nav>
         </div>
       </header>
   <!-- END: Header-->
